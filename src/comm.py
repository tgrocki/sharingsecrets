#
#	This file contains the backend logic of sharingsecrets
#		-sending messages
#		-recieving messages
#		-save states
#


from gnupg import GPG

import traceback
import os
import httplib, urllib
import json
import copy
import time


#
#	create a new user working with this installation
#	parameter:
#		the nickname of the user
#		the passphrase for the user
#		the server to live on
#	return:
#		the userid of the generated user 
#
def createUser(name,passphrase,server):
	#generate the key for the user
	gpg = GPG()
	keyInput = gpg.gen_key_input(passphrase=passphrase,Key_type="RSA",Name_Comment="Key for SharingSecrets-password manager",Name_Real=name,Name_Email="fake@fake.de",Key_Length="4096")
	fingerprint = str(gpg.gen_key(keyInput))
	#TODO:expiration date

	#add key to server
	#hack: wait a minute to get the key accepted on server
	time.sleep(60)
	key = str(gpg.export_keys(fingerprint))[:-1]
	params = urllib.urlencode({'key':key})
	data = urllib.urlopen("http://"+server+"/addkey.php",params)

	return fingerprint

class sharingsecretsBackend(object):

	#
	#       load application data that was writen into a crypted file
	#	parameter:
	#	return:
	#
	def __init__(self,user,settings=[]):
		self.glob_data = {"revert":0,"invited":{},"pending":{},"cache":[],"pws":[],"pwsnocache":[],"users":{},"servers":{}}
		self.glob_username = user.username
		self.glob_userid   = user.userid
		self.glob_server   = user.server
		self.glob_passphrase = user.passphrase
		self.__user = user

		try:
			#load config
			f = open("config/"+self.glob_userid+"/sharingsecrets.conf",'r')
			self.glob_config = json.loads(f.read())

		except: 
			self.glob_config = {"updateFreq":30,"warnlevel":3}

		try:
			#open file
			gpg = GPG()
			f = open("data/"+self.glob_userid+"/sharingsecrets/data.gpg",'r')
			crypted = f.read()
        
			#decrypt and store data
			uncrypted = str(gpg.decrypt(crypted,passphrase=self.glob_passphrase))
			self.glob_data = json.loads(uncrypted)
		except:
			self.glob_data["users"][self.glob_userid] = {}
			self.glob_data["users"][self.glob_userid]["name"] = self.glob_username
			self.glob_data["users"][self.glob_userid]["server"] = self.glob_server

			#initialize the server
			self.glob_data["servers"][self.glob_server] = {}
			self.glob_data["servers"][self.glob_server]["lastid"] = 0
			self.glob_data["servers"][self.glob_server]["cache"] = []

			#set up root folders
			for user in self.glob_data["users"].keys():
				self.glob_data["pwsnocache"].append({"type":"dir","active":0,"name":user,"open":1,"content":[],"realpath":[],"share":[],"inheritedShare":[user],"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":[],"foreignInheritedRoShare":[]})
			
			#create message container
			self.glob_data["messages"] = {}
			self.glob_data["messages"][self.glob_server] = {}
			
	#
	#	(help)
	#	generate a password
	#	parameter:
	#		the length of the password
	#		a string of chars the password can consist of
	#	return:
	#		the password
	#
	def generatePassword(self,size=32, chars=''):
		import string
		import random

		password = ''
		
		if chars == '':
			chars = string.ascii_lowercase + string.ascii_uppercase + string.digits

		while size:
			password=password+random.choice(chars)
			size = size - 1

		return password

	#
	#	(help)
	#	The function for adding a new Partner
	#	parameter:
	#		the keyid of the partner
	#		the server the partner lives on
	#	return:
	#
	def addPartnerKey(self,keyid,server):
		#get the key from the gpg wallet 
		gpg=GPG()
		for key in gpg.list_keys():
			if (key["fingerprint"] == keyid):
				#get the name for the new key
				name = key["uids"][0][0:-15]
				#TODO:prevent name duplication

		#add the new key to the known users
		self.glob_data["users"][keyid] = {}
		self.glob_data["users"][keyid]["name"] = name
		self.glob_data["users"][keyid]["server"] = server

		#remember the server of the key if unknown
		if not server in self.glob_data["servers"]:
			self.glob_data["servers"][server] = {}
			self.glob_data["servers"][server]["lastid"] = 0
			self.glob_data["servers"][server]["cache"] = []
			self.glob_data["messages"][server] = {}

		#create base dir for the new key
		self.glob_data["pwsnocache"].append({"type":"dir","name":keyid,"open":1,"content":[],"realpath":[],"share":[],"inheritedShare":[keyid],"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":[],"foreignInheritedRoShare":[]})

	#
	#	(message)
	#	a negative answer to an request for partnership
	#	paramter:
	#		the keyid of the partner
	#	return:
	#
	def denyInvite(self,keyid):
		#delete the invitation
		if keyid in self.glob_data["invited"].keys():
			del self.glob_data["invited"][keyid]

	#
	#	(message)
	#	a positive answer to an request for partnership
	#	parameter:
	#		the keyid of the partner
	#	return:
	#	
	def ackInvite(self,keyid):
		#add the new partner
		if (not keyid in self.glob_data["users"].keys()) and (keyid in self.glob_data["invited"].keys()):
			self.addPartnerKey(keyid,self.glob_data["invited"][keyid]["server"])
		
		#remove invitiation concerning the new partner
		if keyid in self.glob_data["invited"].keys():
			del self.glob_data["invited"][keyid]
		if keyid in self.glob_data["pending"].keys():
			del self.glob_data["pending"][keyid]

	#
	#	(action)
	#	send a notification-message
	#	parameter:
	#		the message to send
	#		the reciepients of the message
	#	return:
	#
	def sendNotification(self,message,users):
		#send message
		for user in users:
			print("send message")
			self.cacheMessage({"cmd":"notification","message":message},self.glob_data["users"][user]["server"],[user])

	#
	#	(action)
	#	delete notification
	#	parameter:
	#		the messageid to delete
	#	return:
	#
	def deleteNotification(self,messageId):
		#send message to yourself to delete notification-message
		self.cacheMessage({"cmd":"deleteNotification","message":message},self.glob_server,[self.glob_userid])

	#
	#	(action)
	#	deny an request for partnership
	#	parameter:
	#		the keyid of the partner
	#	return:
	#	
	def denyPartner(self,keyid):
		#check if Invitation is still there
		if keyid in self.glob_data["invited"].keys():
			#send a deny message
			self.cacheMessage({"cmd":"denyInvite","name":self.glob_userid,"fingerprint":self.glob_userid},self.glob_data["invited"][keyid]["server"],[keyid],noCrypt=True)

			#send a reciepe for the deny to yourself
			self.cacheMessage({"cmd":"denyReceipt","key":keyid},self.glob_server,[self.glob_userid])

	#
	#	(action)
	#	Accept a request for partnership
	#	parameter:
	#		the keyid of the partner
	#	return:
	#	
	def acceptPartner(self,keyid):
		if keyid in self.glob_data["pending"].keys():

			#remember the server of the key if unknown
			server = self.glob_data["pending"][keyid]["server"]
			if not server in self.glob_data["servers"]:
				self.glob_data["servers"][server] = {}
				self.glob_data["servers"][server]["lastid"] = 0
				self.glob_data["servers"][server]["cache"] = []
				self.glob_data["messages"][server] = {}

			gpg=GPG()
			key = str(gpg.export_keys(self.glob_userid))[:-1]
			params = urllib.urlencode({'key':key})
			data = urllib.urlopen("http://"+server+"/addkey.php",params)

			#send out acknowledgement to partner
			gpg=GPG()
			pubkey = gpg.export_keys(self.glob_userid)
			self.cacheMessage({"cmd":"ackInvite","key":pubkey,"name":self.glob_userid,"fingerprint":self.glob_userid},self.glob_data["pending"][keyid]["server"],[keyid],noCrypt=True)

			#send a reciepe for the ack to yourself
			self.cacheMessage({"cmd":"ackReceipt","key":keyid},self.glob_server,[self.glob_userid])

	#
	#	(message)
	#	Handle an request for partnership from another user
	#	parameter:
	#		the keyid of the partner
	#		the server the partner lives on
	#	return:
	#
	def getPartnerInvite(self,keyid,server):
		if (not keyid in self.glob_data["users"].keys()) and (not keyid in self.glob_data["pending"].keys()):
			#save the request for partnership
			self.glob_data["pending"][keyid] = {}
			self.glob_data["pending"][keyid]["server"] = server

	#
	#	(action)
	#	send an request forpartnership to another user
	#	parameter:
	#		the username of thepartner to invite
	#	return:
	#
	def invite(self,username):
		#extract keyid and server from username
		username = username.split('@', 1 );
		if (len(username) != 2):
			return
		keyid = username[0]
		server = username[1]

		#remember the server of the key if unknown
		if not server in self.glob_data["servers"]:
			self.glob_data["servers"][server] = {}
			self.glob_data["servers"][server]["lastid"] = 0
			self.glob_data["servers"][server]["cache"] = []
			self.glob_data["servers"][server] = {}

		gpg=GPG()
		key = str(gpg.export_keys(self.glob_userid))[:-1]
		params = urllib.urlencode({'key':key})
		data = urllib.urlopen("http://"+server+"/addkey.php",params)

		#send invite containing your public key
		gpg=GPG()
		pubkey = gpg.export_keys(self.glob_userid)
		self.cacheMessage({"cmd":"invite","key":pubkey,"fingerprint":self.glob_userid,"name":self.glob_userid,"server":self.glob_server},server,[keyid],noCrypt=True)
		
		#send a reciepe for the invite to yourself
		self.cacheMessage({"cmd":"inviteReceipt","key":keyid,"server":server},self.glob_server,[self.glob_userid])

	#
	#	(help)
	#	get the Auhthentification tokens for a specific action
	#	an authentification token contains a chain of signed messages, proving that this action was valid at some point in time
	#	parameter:
	#		the item to collect the authentification tokens for
	#	return:
	#		the authentification token
	#
	def getAuth(self,what):
		auth = []
		
		#set the root directory
		dirnow=self.glob_data["pws"]

		#loop through the path hierachy
		for step in what["realpath"]:

			#select the current directory
			for item in dirnow:
				if item["name"]==step:

					#add the authentificationtoken to the array
					auth = item["auth"]+auth

					#step into the current directory
					dirnow = item["content"]

		#add the authentificationtoken for the current item to the array
		auth += what["auth"]

		return auth

	#
	#	(help)
	#	get the authorisation messages contained in a messge-bundle
	#	parameter:
	#		the messeges to extract the authorisation messages from
	#	return:
	#		the authorisation messages
	#
	def getSubmessages(self,messages):
		submessages = []
		
		#loop through the message-packages in the bundle
		for message in messages:
			#unpack the message-package
			messageContent = json.loads(message.split('\n')[3])

			#call itself recursevly to get all authorisation messages
			submessages += self.getSubmessages(messageContent["auth"]) + [message]

		return submessages
			
	#
	#	(help)
	#	check the validity of an authentication token for a command and keyid
	#	parameter:
	#		the command to validate 
	#		the keyid to validate the command for
	#
	def checkAuth(self,cmd,keyid):
		authArray = {}
		baseAuthArray = authArray
		allAuth = []
		
		#try to use passwords with cache if possible
		dirnow = self.glob_data["pws"]
		if not dirnow:
			dirnow = self.glob_data["pwsnocache"]

		#build directory tree for saving permissions
		for step in list(cmd["path"])+[cmd["name"]]:
			authArray["name"] = step
			authArray["child"] = {}
			authArray["authenticated"] = []

		#TODO: what's this and what to do with it?
		#        for item in dirnow:
		#                if item["name"]==step:
		#                        #summ the auth bits:
		#                        allAuth += item["share"]+item["inheritedShare"]
		#                        if item["type"] == "dir":
		#                                dirnow = item["content"]
		#                        break
		#        authArray["authenticated"] = list(set(allAuth))

			authArray = authArray["child"]
		
		#add the root permission
		baseAuthArray["authenticated"] = baseAuthArray["name"]

		#parse the messages
		gpg = GPG()

		#exract the submessages
		submessages = self.getSubmessages(cmd["auth"])

		#add the authorisations to the permission tree 
		for submessage in submessages:
			#reset pointer
			authArray = baseAuthArray
			writeAuthArray = baseAuthArray

			#get fingerprint
			fingerprint = gpg.verify(submessage).fingerprint

			#check validity of the message
			if gpg.verify(submessage) and fingerprint in self.glob_data["users"].keys():

				#extract message content
				submessage = json.loads(submessage.split('\n')[3])

				#check for authentication
				if (submessage["cmd"] == "addkey" or submessage["cmd"] == "addinheritedkey"):
					failed = 0
					authenticated = 0

					#check whether the path is correct 
					for step in submessage["path"]+[submessage["name"]]:
						if authArray["name"] == step:

							#check whether fingerprint is allowed to write
							if fingerprint in authArray["authenticated"]:
								authenticated = 1
							writeAuthArray = authArray 
							authArray = authArray["child"]
						else:
							failed = 1
							break

					#ommit if invalid or unauthorisized
					if failed or not authenticated:
						continue

						#save the permission to the permission tree
						writeAuthArray["authenticated"] += [submessage["keyid"]]
						
			#loop trough tree and check if requested key is there
			authArray = baseAuthArray
			authenticated = 0
			while authArray:
				if keyid in authArray["authenticated"]:
					authenticated = 1
				authArray = authArray["child"]

			#return whether authentification token was valid
			if authenticated:
				return 1 
			else:
				return 0

	#
	#       gets the directories from an array
	#       parameter:
	#               the array
	#       return:
	#               a list of directories
	#
	def getDirsFromArray(self,array):
		#return the directories
		result = []
		for item in array:
			if item["type"] == "dir":
				result.append(item)
		return result 

	#
	#       get password from an array
	#       parameter:
	#               the array
	#       return:
	#               a list of passwords
	#
	def getPwsFromArray(self,array):
		#get the passwords
		result = []
		for item in array:
			if item["type"] == "pw":
				result.append(item)
		return result 

	#
	#	gets password from an array and the contained subdirectories
	#	parameter:
	#		the array
	#	return:
	#		a list of passwords
	#
	def getAllPwsFromArray(self,array):
		result = []
		for item in array:
			if item["type"] == "pw":
				result.append(item)
			if item["type"] == "dir":
				result = result + self.getAllPwsFromArray(item["content"])
		return result

	#
	#	gets the locks from an array
	#	parameter:
	#		the array
	#	return:
	#		a list of locks
	#
	def getLocksFromArray(self,array):
		result = []
		for item in array:
			if item["type"] == "lock":
				result.append(item)
		return result

	#
	#	get keys from an array
	#	parameter:
	#		the array
	#	return:
	#		a list of keys
	#
	def getKeysFromArray(self,array):
		#get the keys
		result = []
		for item in array:
			if item["type"] == "key":
				result.append(item)
		return result

	#
	#       save the data of the session in an crypted file
	#	parameter:
	#	return:
	#               
	def saveData(self):
		#prepare the file to save to
		gpg = GPG()
		f = open("data/"+self.glob_userid+"/sharingsecrets/data.gpg",'w')

		#store crypted data
		uncrypted = json.dumps(self.glob_data)
		crypted = gpg.encrypt(uncrypted,self.glob_userid,always_trust=True)
		f.write(str(crypted))
		f.close()

		# store config
		f = open("config/"+self.glob_userid+"/sharingsecrets.conf",'w')
		f.write(json.dumps(self.glob_config))
		f.close()
	#
	#       load messages from cache to user view
	#	parameter:
	#	return:
	#
	def loadCached(self):
		#loop through messeges in chache
		for server in self.glob_data["servers"]:
			for entry in self.glob_data["servers"][server]["cache"]:
				#construct message from cache
				entry[0]["target"] = self.glob_userid
				message = {"message":json.dumps(entry[0]),"sender":self.glob_userid}

				#filter for messages for yourself
				if self.glob_userid in entry[1]:
					
					#parse the message for user-view
					self.handleMessage(message,self.glob_data["pws"],entry[2])
		

	#
	#       try to send data that was not submited yet
	#	parameter:
	#	return:
	#
	def writeCached(self):
		#try to send each cached message
		for server in self.glob_data["servers"]:
			if self.sendMessage(self.glob_data["servers"][server]["cache"],server):
				self.glob_data["servers"][server]["cache"] = []

	#
	#       update the applicationdata
	#	parameter:
	#		the highest id to fetch (0)
	#			0:fetch everything
	#	return:
	#
	def update(self,limit=0):
		print("update")
		#try to send cached messages
		self.writeCached()
		
		#fetch messages from each server
		numMesg = 0
		for server in self.glob_data["servers"].keys():
			#try to fetch messages from selected server
			try:
				count = self.getMessages(self.glob_data["servers"][server]["lastid"],limit,server)        
				print(server+" "+str(count)+" neue Nachrichten")
				numMesg += count

			#print notification if server doesn't respond
			except Exception, inst:
				print(server+" reagiert nicht")
				traceback.print_exc()

		#update application data
		self.glob_data["pws"] = copy.copy(self.glob_data["pwsnocache"])
		self.loadCached()

		#save application data to file
		self.saveData()

		#start another update if messages were found, to ensure reading a whole "burst" of messages
		if numMesg and not limit:
			self.update()

	#
	#       get all user ids
	#	parameter:
	#       return:
	#               a list of ids
	#
	def getAllUserIDs(self):
		#return the user ids
		ids = []
		for value in self.glob_data["users"].keys():
			ids.append(value)
		return ids

	#
	#       cache a message
	#       parameter:
	#               the message
	#		the server to send the message to
	#               the recipients
	#		whether the message should be send encrypted (False)
	#			False: crypted send 
	#			True: crypted send 
	#	return:
	#               
	def cacheMessage(self,message,server,users,noCrypt=False):
		#write message to cache
		self.glob_data["servers"][server]["cache"].append((message,users,server,noCrypt))

		#save application data
		self.saveData()

	#
	#       send a message to other users
	#       parameter:
	#               the messages
	#               the server to send messages to
	#       return:
	#               whether the messages could be delivered
	#                       False: wasn't delivered
	#                       True: was delivered
	#
	def sendMessage(self,entries,server):
		#try to send the messages
		try:
			gpg = GPG()
			
			#introduce to server if client may be unknown
			if not server in self.glob_data["servers"].keys():
				params = urllib.urlencode({'key':gpg.export_keys(self.glob_userid)})
				data = urllib.urlopen("http://"+server+"/addkey.php",params)

			#send the messages
			messagesOut = []
			for entry in entries:
				#extract message and recipients
				message = entry[0]
				users = entry[1]
				noCrypt = entry[3]

				#encrypt the message for each recipient
				for user in users:
					#crypt and sign the message
					message["target"]=user
					message["targetServer"]=server
					gpgPrefs = {"keyid":self.glob_userid,"passphrase":self.glob_passphrase}
					tmpMessage = gpg.sign(json.dumps(message),**gpgPrefs)
					#ommit encryption if needed
					if not noCrypt:
						outMessage = str(gpg.encrypt(str(tmpMessage),user,always_trust=True))
					else:
						outMessage = str(tmpMessage)
					
					messagesOut.append([outMessage,user])

				#sign mesage-bundle and send it to server
				gpgPrefs = {"keyid":self.glob_userid,"passphrase":self.glob_passphrase}
				signed = gpg.sign(json.dumps(messagesOut),**gpgPrefs)
				params = urllib.urlencode({'message':signed})
				data = urllib.urlopen("http://"+server+"/distribute2.php",params)
				messagesOut = []

				#open mesage bundle
				messages = data.read()

			return True
		except Exception, inst:
			traceback.print_exc()
			return False

	#
	#	(action)
	#       send the message for deleting an item
	#       parameter:
	#               the path of the item
	#               the item
	#               the recipients
	#	return:
	#
	def deleteItem(self, realpath, item, users):
		#cache message
		self.cacheMessage({"cmd":"delete","path":realpath,"name":item},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#	send the message for storing an key
	#	parameter:
	#		the name of the key
	#		the key itself
	#		the keyid
	#		the host
	#		a comment to describe the key
	#
	def saveExternalKey(self,users,realpath,password,name,key,keyid,host,comment,auth=""):
		self.cacheMessage({"cmd":"saveexternalkey","password":password,"name":name,"key":key,"keyid":keyid,"path":realpath,"host":host,"comment":comment,"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#	send the message for creating a new lock
	#	parameter:
	#		the password to save
	#		the username to save
	#		the host to save
	#		the comment to save
	#		the number of partners needed to open the lock
	#		the partners allowed toopen the lock
	#		the partners to ignore in this round
	#	return:
	#		the generated lock
	#
	def createLock(self,password,username,host,comment,level,partners,ignore):
		gpg = GPG()

		#loop through all the partners
		result = {"messages":{},"level":level,"partners":partners,"unlocked":ignore}
		for partner in partners:

			#omit partners to be ignored
			if not partner in ignore:
				
				#create a lock recursively with changed parameters, if still more partners are needed
				if level > 1:
					#save the lock as the message for this user
					message = self.createLock(password,username,host,comment,level-1,partners,ignore+[partner])
				#save the data in a message if no more recursion is needed
				else:
					message = {"level":0,"password":password,"username":username,"host":host,"comment":comment,"partners":partners,"unlocked":ignore+[partner]}

				#add the encrypted message to theresukting lock
				result["messages"][partner] = str(gpg.encrypt(json.dumps(message),partner,always_trust=True))
		return result
			
	#
	#	(action)
	#	send the message for creating a new lock
	#	paramater:
	#		the password to save
	#		the username to save
	#		the user to recieve the lock
	#		the path of the lock
	#		the name of the lock
	#		the host to send the lock to
	#		the comment to save
	#		the partners allowed toopen the lock
	#		the number of partners needed to open the lock
	#		the authentification token to save the lock
	#	return:
	#
	def saveLock(self,password,username,users,realpath,name,host,comment,partner,level,auth=""):
		#generate the lockmessage for yourself
		message = self.createLock(password,username,host,comment,level,partner,[])

		#loop throut the users
		for user in users:

			#build the lock for each recipient
			lock = json.dumps({"messages":[],"partners":partner,"unlocked":[],"level":level-1})
			
			#fetch and add the lock content for the recipient, if he's allowed to open the lock
			if user in partner:
				lock = message["messages"][user]

			#cache the message for this user
			self.cacheMessage({"cmd":"savelock","path":realpath,"name":name,"lock":lock,"auth":auth},self.glob_data["users"][realpath[0]]["server"],[user])

	#
	#	(action)
	#	unlock a lock
	#	parameter:
	#		the path of the lock
	#		the name of the lock
	#		the lock itself
	#		the reciepients
	#	return:
	#
	def unlock(self,realpath,name,lock,users):
		#send new lockmessage still more users are needed
		if lock["level"]>0:
			#loop through each recipient
			for user in users:
				#fetch lock if reciepient is allowed to open the lock
				if user in lock["messages"].keys():
					lockMessage = lock["messages"][user]
				#create fake lock if the user is not allowed
				else:
					lockMessage = json.dumps({"messages":[],"partners":lock["partners"],"unlocked":lock["unlocked"],"level":lock["level"]-1})
				#cache the new lockmessage
				self.cacheMessage({"cmd":"savelock","path":realpath,"name":name,"lock":lockMessage,"auth":"","messages":[]},self.glob_data["users"][realpath[0]]["server"],[user])
		else:
			self.savePassword(lock["password"],lock["username"],users,realpath,name,lock["host"],lock["comment"]);


	#
	#	(action)
	#       send the message for saving a message
	#       parameter:
	#               the password
	#               the username for the password
	#               the recipients
	#               the path of the pasword
	#               the name of the pasword item
	#               the host for the password
	#               the comment for the password
	#		the authentification token for the password
	#	return:
	#
	def savePassword(self,password,username,users,realpath,name,host,comment,auth=""):
		#try to send message
		self.cacheMessage({"cmd":"savepw","username":username,"password":password,"name":name,"path":realpath,"host":host,"comment":comment,"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#       send the message for saving a directory
	#       parameter:
	#               the name of the pasword item
	#               the path of the pasword
	#               the recipients
	#		the authenticifation token for the password
	#	return:
	#
	def saveDirectory(self,name,realpath,users,auth=""):
		#try to send message
		self.cacheMessage({"cmd":"savedir","name":name,"path":realpath,"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#       add access for a key
	#       parameter:
	#               the name of the item to remove access from
	#               the path of the item to remove access from
	#               the recipients
	#               the keyid of the key to add
	#		the authenticifation token for the password
	#	return:
	#
	def addKey(self,name,realpath,users,keyid,auth=""):
		#try to send message
		gpg = GPG()
		self.cacheMessage({"cmd":"addkey","name":name,"path":realpath,"keyid":keyid,"key":gpg.export_keys(keyid),"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(acrion)
	#	add read-only access for a key
	#	parameter:
	#		the name of the item to give access to
	#		the path of the item to give access to
	#		the recipients
	#		the keyid of the key to add
	#		the authenticifation token for the password
	#	return:
	#
	def addRoKey(self,name,realpath,users,keyid,auth=""):
		#try to send message
		gpg = GPG()
		self.cacheMessage({"cmd":"addrokey","name":name,"path":realpath,"keyid":keyid,"key":gpg.export_keys(keyid),"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#	add a new partner to a partner
	#	parameter:
	#		the recipients
	#		the keyid
	#	return:
	#
	def addPartner(self,users,keyid):
		gpg = GPG()
		for user in users:
			self.cacheMessage({"cmd":"addpartner","keyid":keyid,"key":gpg.export_keys(keyid)},self.glob_data["users"][user]["server"],[user])

	#
	#	(action)
	#	add an inherited read-only access for a key
	#	parameter:
	#		the name of the item to give access to
	#		the path of the item to give access to
	#		the recipients
	#		the keyid of the key to add
	#		the authenticifation token for the password
	#	return:
	#
	def addInheritedRoKey(self,name,realpath,users,keyid,auth=""):
		#try to send message
		gpg = GPG()
		self.cacheMessage({"cmd":"addinheritedRokey","name":name,"path":realpath,"keyid":keyid,"key":gpg.export_keys(keyid),"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)


	#
	#	(action)
	#	add an inherited access for a key
	#	parameter:
	#		the name of the item to give access to
	#		the path of the item to give access to
	#		the recipients
	#		the keyid of the key to add
	#		the authenticifation token for the password
	#	return:
	#
	def addInheritedKey(self,name,realpath,users,keyid,auth=""):
		#try to send message
		gpg = GPG()
		self.cacheMessage({"cmd":"addinheritedkey","name":name,"path":realpath,"keyid":keyid,"key":gpg.export_keys(keyid),"auth":auth},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#       remove access for a key
	#       parameter:
	#               the name of the item to remove access from
	#               the path of the item to remove access from
	#               the recipients
	#               the keyid of the key to remove
	#	return:
	#
	def deleteKey(self,name,realpath,users,keyid):
		#try to send message
		self.cacheMessage({"cmd":"removekey","name":name,"path":realpath,"keyid":keyid},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#       remove read-only access for a key
	#       parameter:
	#               the name of the item to remove access from
	#               the path of the item to remove access from
	#               the recipients
	#               the keyid of the key to remove
	#	return:
	#
	def deleteRoKey(self,name,realpath,users,keyid):
		#try to send message
		self.cacheMessage({"cmd":"removerokey","name":name,"path":realpath,"keyid":keyid},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(action)
	#       remove inherited access for a key
	#       parameter:
	#               the name of the item to remove access from
	#               the path of the item to remove access from
	#               the recipients
	#               the keyid of the key to remove
	#	return:
	#
	def deleteInheritedKey(self,name,realpath,users,keyid):
		#try to send message
		self.cacheMessage({"cmd":"removeinheritedkey","name":name,"path":realpath,"keyid":keyid},self.glob_data["users"][realpath[0]]["server"],users)

	#
	#	(message)
	#	notification that a invite was send at some point of time
	#	parameter
	#               the command
	#               the section of application data to write to
	#               the sender
	#	return:
	#
	def inviteReceipt(self,cmd,sender):
		#guess missing server for backwards compability
		if not "server" in cmd:
			cmd["server"] = self.glob_server
			
		#add the invite to the user-view
		if sender == self.glob_userid:
			self.glob_data["invited"][cmd["key"]] = {}
			self.glob_data["invited"][cmd["key"]]["server"] = cmd["server"]

	#
	#	(message)
	#	notification that an invite was accepted at some point of time
	#	parameter
	#               the command
	#               the sender
	#	return:
	#
	def ackReceipt(self,cmd,sender):
		#only accept messages from yourself
		if not sender == self.glob_userid:
			return
		
		#check if there is an corresponding invite
		if not cmd["key"] in self.glob_data["pending"].keys():
			return 
		
		#add key to partners
		self.addPartnerKey(cmd["key"],self.glob_data["pending"][cmd["key"]]["server"])

		#delete invitations concerning the new partner
		if cmd["key"] in self.glob_data["pending"].keys():
			del self.glob_data["pending"][cmd["key"]]
		if cmd["key"] in self.glob_data["invited"].keys():
			del self.glob_data["invited"][cmd["key"]]

	#
	#	(message)
	#	notification that an invite was denied at some point of time
	#	parameter
	#               the command
	#               the sender
	#	return:
	#	
	def denyReceipt(self,cmd,sender):
		#only accept messages from yourself
		if not sender == self.glob_userid:
			return
		
		#delete invitations concerning the new partner
		if cmd["key"] in self.glob_data["pending"].keys():
			del self.glob_data["pending"][cmd["key"]]
	#
	#       delete item from local application data
	#       parameter:
	#               the command
	#               the section of application data to write to
	#               the sender
	#	return:
	# 
	def deleteItemLocal(self,cmd,what,sender):
		#open directory
		dirnow = what
		for element in cmd["path"]:
			found = 0
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow = directory["content"]
					break
		
		#delete the item
		for item in dirnow:
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"]:
					print("illegal request! (no permission to delete item)")
					return

				dirnow.remove(item)
				break

	def storeKeyLocal( self, path, password="", key="", keyid= "",host="",comment="" ):
		print("called")
		if not path[0] == "/":
			return

		print(path)

		path = path.split("/")
		keyName = path[-1]
		path = path[1:-1]
		
		dirnow = self.glob_data["pws"]
		for element in path:
			found = False
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow=directory["content"]
					found=True
			if not found:
				return None

		print("save")
		self.saveExternalKey(list(set(directory["share"]+directory["inheritedShare"]+directory["roShare"]+directory["inheritedRoShare"])),path,password,keyName,key,keyid,host,comment)
		
	def readKeyLocal( self,path ):
		if not path[0] == "/":
			return None
		
		path = path.split("/")
		keyName = path[-1]
		path = path[1:-1]
		
		dirnow = self.glob_data["pws"]
		for element in path:
			found = False
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow=directory["content"]
					found=True
			if not found:
				return None

		
		found = False
		for key in self.getKeysFromArray(dirnow):
			if key["name"] == keyName:
				found=True
				break
		if not found:
			return None

		return key

	def readPwLocal( self,path ):
		if not path[0] == "/":
			return None

		path = path.split("/")
		password = path[-1]
		path = path[1:-1]
		
		dirnow = self.glob_data["pws"]
		for element in path:
			found = False
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow=directory["content"]
					found=True
			if not found:
				return None

		
		found = False
		for pw in self.getPwsFromArray(dirnow):
			if pw["name"] == password:
				password = pw
				found=True
				break
		if not found:
			return None

		return password

					
	def saveExternalKeyLocal(self,cmd,what,sender,message=""):
		#open/create directory
		dirnow = what
		realpath = []
		usersShare = []
		usersRoShare = []
		#walk the path to collect the permissions
		for element in cmd["path"]:
			found = 0
			#try to open directory
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:

					#add ownerpermission to root elements
					if directory["realpath"] == []:
						usersShare.append(directory["name"])

					#change directory
					realpath.append(directory["name"])
					found = 1
					dirnow = directory["content"]

					#remember permissions
					usersShare = directory["share"]+directory["inheritedShare"]
					usersRoShare = directory["roShare"]+directory["inheritedRoShare"]
					break
			#try to create a new directory if it was not found
			if not found:
				#check authorisation token
				if not sender in usersShare and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to create directory for password)")
					return
				
				#create directory
				dirnow.append({"type":"dir","name":element,"open":1,"content":[],"realpath":list(realpath),"share":[],"inheritedShare":list(set(usersShare)),"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":[]})

				#change directory
				realpath.append(element)
				dirnow = dirnow[-1]["content"]

		#remove password or lock with same name
		lastShare = []
		lastRoShare = []
		lastforeignInheritedShare = []
		lastforeignInheritedRoShare = []
		for item in self.getPwsFromArray(dirnow)+self.getLocksFromArray(dirnow)+self.getKeysFromArray(dirnow):
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"] and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to delete old password))")
					return

				#carry permissions from old pw
				lastShare = item["share"]
				lastRoShare = item["roShare"]
				lastforeignInheritedShare = item["foreignInheritedShare"]
				lastforeignInheritedRoShare = item["foreignInheritedRoShare"]

				#delete password
				dirnow.remove(item)
				break

		#check access permission
		if not sender in usersShare+lastShare and not self.checkAuth(cmd,sender):
			print("illegal request! (no permission to create password)")
			return

		#save external key
		dirnow.append({"type":"key","password":cmd["password"],"name":cmd["name"],"key":cmd["key"],"keyid":cmd["keyid"],"realpath":cmd["path"],"host":cmd["host"],"comment":cmd["comment"],"share":list(set(lastShare)),"inheritedShare":list(set(usersShare)),"foreignInheritedShare":list(set(lastforeignInheritedShare)),"auth":[],"roShare":list(set(lastRoShare)),"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":list(set(lastforeignInheritedRoShare))})
		
	#
	#       save password to local application data
	#       parameter:
	#               the command
	#               the section of application data to write to0B381DB90F0725807DC48755FF8D2CE
	#               the sender
	#		the original message
	#	return:
	# 
	def savePwLocal(self,cmd,what,sender,message=""):
		#open/create directory
		dirnow = what
		realpath = []
		usersShare = []
		usersRoShare = []
		#walk the path to collect the permissions
		for element in cmd["path"]:
			found = 0
			#try to open directory
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:

					#add ownerpermission to root elements
					if directory["realpath"] == []:
						usersShare.append(directory["name"])

					#change directory
					realpath.append(directory["name"])
					found = 1
					dirnow = directory["content"]

					#remember permissions
					usersShare = directory["share"]+directory["inheritedShare"]
					usersRoShare = directory["roShare"]+directory["inheritedRoShare"]
					break
			#try to create a new directory if it was not found
			if not found:
				#check authorisation token
				if not sender in usersShare and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to create directory for password)")
					return
				
				#create directory
				dirnow.append({"type":"dir","name":element,"open":1,"content":[],"realpath":list(realpath),"share":[],"inheritedShare":list(set(usersShare)),"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":[]})

				#change directory
				realpath.append(element)
				dirnow = dirnow[-1]["content"]

		#remove password or lock with same name
		lastShare = []
		lastRoShare = []
		lastforeignInheritedShare = []
		lastforeignInheritedRoShare = []
		for item in self.getPwsFromArray(dirnow)+self.getLocksFromArray(dirnow)+self.getKeysFromArray(dirnow):
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"] and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to delete old password))")
					return

				#carry permissions from old pw
				lastShare = item["share"]
				lastRoShare = item["roShare"]
				lastforeignInheritedShare = item["foreignInheritedShare"]
				lastforeignInheritedRoShare = item["foreignInheritedRoShare"]

				#delete password
				dirnow.remove(item)
				break

		#check access permission
		if not sender in usersShare+lastShare and not self.checkAuth(cmd,sender):
			print("illegal request! (no permission to create password)")
			return

		#save password
		dirnow.append({"type":"pw","name":cmd["name"],"username":cmd["username"],"password":cmd["password"],"realpath":cmd["path"],"host":cmd["host"],"comment":cmd["comment"],"share":list(set(lastShare)),"inheritedShare":list(set(usersShare)),"foreignInheritedShare":list(set(lastforeignInheritedShare)),"auth":[],"roShare":list(set(lastRoShare)),"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":list(set(lastforeignInheritedRoShare))})

	#
	#	(message)
	#	add a lock to local application
	#       parameter:
	#               the command
	#               the section of application data to write to
	#               the sender
	#	return:
	# 
	def addLockLocal(self,cmd,what,sender):
		dirnow = what
		usersShare = []
		realpath = []
		found = 0
		#walk the path to collect the permissions
		for element in cmd["path"]:

			#try to open directory
			found = 0
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:

					#add ownerpermission to root elements
					if directory["realpath"] == []:
						usersShare.append(directory["name"])

					#change directory
					realpath.append(directory["name"])
					found = 1
					dirnow = directory["content"]

					#remember permissions
					usersShare = directory["share"]+directory["inheritedShare"]
					usersRoShare = directory["roShare"]+directory["inheritedRoShare"]
					break

			#try to create a new directory if it was not found
			if not found:
				#check access permission
				if not sender in usersShare and not self.checkAuth(cmd,sender):
					print("illegal request! (no permissin to create dir for lock)")
					return
				
				#create directory
				dirnow.append({"type":"dir","name":element,"open":1,"content":[],"realpath":list(realpath),"share":[],"inheritedShare":list(set(usersShare)),"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":[]})

				#change directory
				realpath.append(element)
				dirnow = dirnow[-1]["content"]

		#remove lock with same name
		for item in self.getPwsFromArray(dirnow)+self.getLocksFromArray(dirnow)+self.getKeysFromArray(dirnow):
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"] and not checkAuth(cmd,sender):
					print("illegal request! (no permission to remove old lock)")
					return

				#change the lock
				try:
					lockMsg = json.loads(cmd["lock"])
				except:
					gpg = GPG()
					lockMsg = json.loads(str(gpg.decrypt(cmd["lock"],passphrase=self.glob_passphrase)))
				item["lock"] = lockMsg
				return

			#check access permission
		if not sender in usersShare and not self.checkAuth(cmd,sender):
			print("illegal request! (no permission to create lock)")
			return

		#save lock
		try:
			lockMsg = json.loads(cmd["lock"])
		except:
			gpg = GPG()
			lockMsg = json.loads(str(gpg.decrypt(cmd["lock"],passphrase=self.glob_passphrase)))
		dirnow.append({"type":"lock","name":cmd["name"],"lock":lockMsg,"realpath":cmd["path"],"share":[],"inheritedShare":list(set(usersShare)),"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":[]})
		
	#
	#	(message)
	#       save directory to local application data
	#       parameter:
	#               the command
	#               the section of application data to write to
	#               the sender
	#	return:
	#
	def saveDirLocal(self,cmd,what,sender):
		#open/create directory
		dirnow = what
		path = cmd["path"]+[cmd["name"]]
		realpath = []
		usersShare = []
		usersRoShare = []
		#loop through the path element
		for element in path:
			found = 0
			#go to the subdirs
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					if directory["realpath"] == []:
						usersShare.append(directory["name"])
					realpath.append(directory["name"])
					found = 1
					parent = directory
					dirnow = directory["content"]
					#carry the share options
					usersShare = directory["share"]+directory["inheritedShare"]
					usersRoShare = directory["roShare"]+directory["inheritedRoShare"]
					break;

			#create directory if necessary
			if not found:
				#check access permission
				if not sender in usersShare and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to create directory)")
					return

				#save dir
				dirnow.append({"type":"dir","name":element,"open":1,"content":[],"realpath":list(realpath),"inheritedShare":list(set(usersShare)),"foreignInheritedShare":[],"share":[],"auth":[],"roShare":[],"inheritedRoShare":list(set(usersRoShare)),"foreignInheritedRoShare":[]})
				parent = dirnow[-1]
				dirnow = dirnow[-1]["content"]
				realpath.append(element)

	#
	#	(help)
	#	add read-only access for a key recursevly
	#	parameter:
	#		the directory to add the read-only access
	#		the keyid
	#	return:
	#
	def addRoKeyLocalRecursive(self,item,keyid):
		#loop through the content of the directory
		for subItem in item["content"]:
			
			#call function recursevly if the item is a directory
			if subItem["type"] == "dir":
				self.addRoKeyLocalRecursive(subItem,keyid) 

			#add the key
			subItem["inheritedRoShare"].append(keyid)
			subItem["inheritedRoShare"] = list(set(subItem["inheritedRoShare"]))

	#
	#	(message)
	#       add key to local application data
	#       parameter:
	#               the command
	#               the section of application data to write to
	#               the sender
	#               whether the permission was inherited (0)
	#			0: not inherited
	#			1: inherited
	#               whether the permission is a read-only permission (0)
	#			0: no read-only
	#			1: read-only
	#               the original message
	#	return:
	#       
	def addKeyLocal(self,cmd,what,sender,inherited=0,ro=0,message=""):
	   try:
		#open directory
		dirnow = what
		for element in cmd["path"]:
			found = 0
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow = directory["content"]
					break
		
		#get item
		for item in dirnow:
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"] and not self.checkAuth(cmd,sender):
					print("illegal request! (no permission to add key)")
					return

				#save authentication for later use
				if not self.glob_userid in item["share"]+item["inheritedShare"] and not ro:
					item["auth"].append(message);

				#add key to partners
				if not cmd["keyid"] in self.glob_data["users"].keys():
					gpg = GPG()
					gpg.import_keys(cmd["key"])

					#guess server for backwards compability
					if not "server" in cmd:
						cmd["server"] = self.glob_server
						
					#add to partners
					self.addPartnerKey(cmd["keyid"],cmd["server"])
					

				#add key to folder
				if not ro:
					if inherited:
						#add the inheritedKey
						item["foreignInheritedShare"].append(cmd["keyid"])
						item["foreignInheritedShare"] = list(set(item["foreignInheritedShare"]))
						item["inheritedShare"].append(cmd["keyid"])
						item["inheritedShare"] = list(set(item["inheritedShare"]))
					else:
						item["share"].append(cmd["keyid"])
						item["share"] = list(set(item["share"]))

					#add key recursive
					if item["type"] == "dir":
						self.addKeyLocalRecursive(item,cmd["keyid"])

				else:
					if inherited:
						#add the inheritedKey
						item["foreignInheritedRoShare"].append(cmd["keyid"])
						item["foreignInheritedRoShare"] = list(set(item["foreignInheritedRoShare"]))
						item["inheritedRoShare"].append(cmd["keyid"])
						item["inheritedRoShare"] = list(set(item["inheritedRoShare"]))
						pass	
					else:
						item["roShare"].append(cmd["keyid"])
						item["roShare"] = list(set(item["roShare"]))

					#add key recursive
					if item["type"] == "dir":
						self.addRoKeyLocalRecursive(item,cmd["keyid"])
					
				break
	   except Exception,e:
		traceback.print_exc()

	#
	#	(help)
	#       add access to a key from a directory and all subitems
	#       parameter:
	#               the item to remove access from
	#               the keyid to add
	#	return:
	#
	def addKeyLocalRecursive(self,item,keyid):
		#loop through the directory
		for subItem in item["content"]:
			#call itself for each contained directory
			if subItem["type"] == "dir":
				self.addKeyLocalRecursive(subItem,keyid) 

			#add the key
			subItem["inheritedShare"].append(keyid)
			subItem["inheritedShare"] = list(set(subItem["inheritedShare"]))

	#
	#	(help)
	#	remove read-only access to a key from a directory and all subitems
	#	parameter:
	#               the item to remove access from
	#               the keyid to add
	#	return:
	#		
	def deleteRoKeyLocalRecursive(self,item,keyid):
		#loop through the directory
		for subItem in item["content"]:
			if not keyid in item["roShare"] and not keyid in item["foreignInheritedRoShare"]:

				#call itself for each contained directory
				if subItem["type"] == "dir":
					self.deleteRoKeyLocalRecursive(subItem,keyid) 

				#add the key
				if keyid in subItem["inheritedRoShare"]:
					subItem["inheritedRoShare"].remove(keyid)

	#
	#	(help)
	#       remove a key from a directory and all subitems
	#       parameter:
	#               the directory to remove the access from
	#               the keyid to remove
	#	return:
	#
	def deleteKeyLocalRecursive(self,item,keyid):
		#loop through the contained items
		for subItem in item["content"]:
			#call the function recursevly, if neccessary
			if not keyid in item["share"]:
				if subItem["type"] == "dir":
					self.deleteKeyLocalRecursive(subItem,keyid) 

			#remove the key
			if keyid in subItem["inheritedShare"]:
				subItem["inheritedShare"].remove(keyid)

	#
	#	(message)
	#       remove key from local application data
	#       parameter:
	#               the command
	#               the section of application data to write to
	#               the sender of the message
	#               whether the permission is a read-only permission (0)
	#			0: no read-only
	#			1: read-only
	#               whether the permission was inherited (0)
	#			0: not inherited
	#			1: inherited
	#	return:
	#       
	def deleteKeyLocal(self,cmd,what,sender,ro=0,inherited=0):
		#open directory
		dirnow = what
		directory = None
		for element in cmd["path"]:
			found = 0
			for directory in self.getDirsFromArray(dirnow):
				if directory["name"] == element:
					dirnow = directory["content"]
					break
		
		#get item
		for item in dirnow:
			if item["name"]==cmd["name"]:
				#check access permission
				if not sender in item["share"]+item["inheritedShare"]:
					print("illegal request! (no permission to remove key)")
					return


				#delete key
				if not ro:
					if inherited:
						if cmd["keyid"] in item["foreignInheritedShare"]:
							item["foreignInheritedShare"].remove(cmd["keyid"])
						if cmd["keyid"] in item["inheritedShare"] and ((directory is None) or not (cmd["keyid"] in directory["inheritedShare"] or cmd["keyid"] in directory["share"])):
							item["inheritedShare"].remove(cmd["keyid"])
					else:
						if cmd["keyid"] in item["share"]:
							item["share"].remove(cmd["keyid"])
					if item["type"] == "dir" and (directory is None or not (cmd["keyid"] in directory["inheritedShare"] or cmd["keyid"] in directory["share"])):
						self.deleteKeyLocalRecursive(item,cmd["keyid"])
				else:
					if inherited:
						if cmd["keyid"] in item["foreignInheritedRoShare"]:
							item["foreignInheritedRoShare"].remove(cmd["keyid"])
						if cmd["keyid"] in item["inheritedRoShare"] and ((directory is None) or not (cmd["keyid"] in directory["inheritedRoShare"] or cmd["keyid"] in directory["roShare"])):
							item["inheritedShare"].remove(cmd["keyid"])
					else:
						if cmd["keyid"] in item["roShare"]:
							item["roShare"].remove(cmd["keyid"])
					if item["type"] == "dir" and (directory is None or not (cmd["keyid"] in directory["inheritedRoShare"] or cmd["keyid"] in directory["roShare"])):
						self.deleteRoKeyLocalRecursive(item,cmd["keyid"])

				break

	#
	# 	(message)
	#	add a new partner to local aplication data
	#	parameter:
	#               the command
	#               the section of application data to write to
	#	return:
	#
	def addPartnerLocal(self,cmd,sender):
		#act only if partner is unknown
		if cmd["keyid"] in self.glob_data["users"].keys():
			return

		#import the key to gpg wallet
		gpg = GPG()
		gpg.import_keys(cmd["key"])

		#guess the server for backwards compability
		if not "server" in cmd:
			cmd["server"] = self.glob_server
			
		#add the key to local application data
		self.addPartnerKey(cmd["keyid"],cmd["server"])

	#
	#	(message)
	#	recieves a notification 
	#	parameter:
	#		the message
	#		the server the notification-message is from
	#		the id of the message containing the notification
	#		the sender of the message
	#
	def addNotification(self,cmd,server,messageId,sender,time):
		self.glob_data["messages"][server][messageId] = {}
		self.glob_data["messages"][server][messageId]["message"] = cmd["message"]
		self.glob_data["messages"][server][messageId]["time"] = time
		self.glob_data["messages"][server][messageId]["sender"] = sender

	#
	#	(message)
	#	delete a notification
	#	parameter:
	#		the message
	#		the server the notification-message is from
	#		the id of the message containing the notification
	#		the sender of the message
	#
	def deleteNotification(self,cmd,server,messageId,sender):
		if sender == self.glob_userid:
			del self.glob_data["messages"][server][messageId]

	#
	#	(message)
	#       handle a message
	#       parameter:
	#               the message
	#               the section of application data to write to
	#		the server the message is from
	#		the original message
	#	return:
	#               
	def handleMessage(self,message,what,server,unchanged=None):
		try:
			#parse message
			cmd = json.loads(message["message"])

			#check adressant, to avoid forwarding messages to fake adressants
			if (cmd["target"] != self.glob_userid):
				print "illegal request (forwarding)!!"
			else:
				#call specific handler
				if (cmd["cmd"] == "savepw"):
					print("savepw")
					self.savePwLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "savedir"):
					print("savedir")
					self.saveDirLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "saveexternalkey"):
					print("saveexternalkey")
					self.saveExternalKeyLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "delete"):
					print("delete")
					self.deleteItemLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "removekey"):
					print("removekey")
					self.deleteKeyLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "removerokey"):
					print("removerokey")
					self.deleteKeyLocal(cmd,what,message["sender"],ro=1)
				elif (cmd["cmd"] == "removeinheritedkey"):
					print("removeinheritedkey")
					self.deleteKeyLocal(cmd,what,message["sender"],inherited=1)
				elif (cmd["cmd"] == "removeinheritedrokey"):
					print("removeinheritedrokey")
					self.deleteKeyLocal(cmd,what,message["sender"],ro=1,inherited=1)
				elif (cmd["cmd"] == "addkey"):
					print("addkey")
					self.addKeyLocal(cmd,what,message["sender"],message=unchanged)
				elif (cmd["cmd"] == "addrokey"):
					print("addrokey")
					self.addKeyLocal(cmd,what,message["sender"],ro=1,message=unchanged)
				elif (cmd["cmd"] == "addinheritedkey"):
					print("addinheritedkey")
					self.addKeyLocal(cmd,what,message["sender"],inherited=1,message=unchanged)
				elif (cmd["cmd"] == "addinheritedRokey"):
					print("addinheritedRokey")
					self.addKeyLocal(cmd,what,message["sender"],inherited=1,ro=1,message=unchanged)
				elif (cmd["cmd"] == "inviteReceipt"):
					print("inviteReceipt")
					self.inviteReceipt(cmd,message["sender"])
				elif (cmd["cmd"] == "ackReceipt"):
					print("ackReceipt")
					self.ackReceipt(cmd,message["sender"])
				elif (cmd["cmd"] == "denyReceipt"):
					print("denyReceipt")
					self.denyReceipt(cmd,message["sender"])
				elif (cmd["cmd"] == "addpartner"):
					print("addpartner")
					self.addPartnerLocal(cmd,message["sender"])
				elif (cmd["cmd"] == "savelock"):
					print("savelock")
					self.addLockLocal(cmd,what,message["sender"])
				elif (cmd["cmd"] == "notification"):
					print("notification")
					self.addNotification(cmd,server,message["id"],message["sender"],message["time"])
				elif (cmd["cmd"] == "deleteNotification"):
					print("deleteNotification")
					self.deleteNotification(cmd,server,message["id"],message["sender"])
				else:
					print("unknown message type")

			#update last message id
			if "id" in message:
				self.glob_data["servers"][server]["lastid"] = message["id"]
		except Exception as inst:
			traceback.print_exc()
			

	#
	#	(message)
	#       get new messages from server
	#       parameter:
	#               the id of the last known message       
	#		the highest id to fetch
	#		the server to get messages from 
	#	return:
	#		the number of messages found
	#
	def getMessages(self,since,limit,server):
		#query the server for messages
		gpg = GPG()
		gpgPrefs = {"keyid":self.glob_userid,"passphrase":self.glob_passphrase}
		sinceSigned = gpg.sign(str(since),**gpgPrefs)
		gpgPrefs = {"keyid":self.glob_userid,"passphrase":self.glob_passphrase}
		limitSigned = gpg.sign(str(limit),**gpgPrefs)
		params = urllib.urlencode({'since':sinceSigned,'limit':limitSigned})
		data = urllib.urlopen("http://"+server+"/get2.php",params)

		#open mesage bundle
		messages = data.read()
		try:
			messages = json.loads(messages)
		except Exception, e:
			print("messages")
			print(messages)
			
		#loop through messages
		for message in messages:
			#decrypt message
			crypt = message["message"] 
			message["message"] = str(gpg.decrypt(message["message"],passphrase=self.glob_passphrase))
			unchanged = message["message"]

			#verify that a partner signed the message
			if gpg.verify(message["message"]) and gpg.verify(message["message"]).fingerprint in self.glob_data["users"].keys():
				#save sender and message
				message["sender"] = gpg.verify(message["message"]).fingerprint
				message["message"] = message["message"].split('\n')[3]

				#call the handler for messages
				self.handleMessage(message,self.glob_data["pwsnocache"],server,unchanged=unchanged)
			#check for invitations if message is not properly signed
			else:
				print("not a message from known key")
				#assume signed but not crypted invitation message
				try:
					#open message content
					command = crypt.split('\n')[3]
					cmd = json.loads(command)

					#import key
					gpg.import_keys(cmd["key"])

					#check signature
					if gpg.verify(crypt) and cmd["fingerprint"]==gpg.verify(crypt).fingerprint:
						#call handler if message is an invitation message
						if (cmd["cmd"]=="invite"):
							self.getPartnerInvite(cmd["fingerprint"],cmd["server"])
						elif (cmd["cmd"]=="ackInvite"):
							self.ackInvite(cmd["fingerprint"])
						elif (cmd["cmd"]=="denyInvite"):
							self.denyInvite(cmd["fingerprint"])
						else:
							print("no valid command for uncrypted messages");
					else:
						print("not a valid message for unknown key")
				except:
					#drop the message
					print("unreadable message")

				#update the message counter
				self.glob_data["servers"][server]["lastid"] = message["id"]
		
		return len(messages)
