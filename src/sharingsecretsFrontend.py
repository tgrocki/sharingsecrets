#! /usr/bin/python

#
#	The graphical Userinterface for sharingsecrets 
#

import traceback
import gtk
import gobject
import getpass
import copy


glob_lineHeight = 18;
glob_editHeight = 25;
glob_indent = 18;
glob_marginTop = 10;
glob_marginLeft = 10;
glob_dirIconSize = 18;
global_pwmanager = None

#
#       the panel for password management
#       
class PasswordPanel(gtk.HPaned):
        #
        #       the constructor
        #
        def __init__(self,pwmanager):
		gtk.HPaned.__init__(self)

		global global_pwmanager
		global_pwmanager = pwmanager
 
                #create sub panels
                global pwListPanel
               	pwListPanel = PasswordListPanel(self)
               	leftPanel = pwListPanel
               	leftPanel.show()
                global pwDetailPanel
                pwDetailPanel = PasswordDetailsPanel(self)
                rightPanel = pwDetailPanel
		#set color
               	rightPanel.show()

                #put sub panels in a spliter       
		self.add1(leftPanel)
		self.add2(rightPanel)
		self.set_position(300)

                #init the timer for checking for new data
		gobject.timeout_add_seconds(global_pwmanager.glob_config["updateFreq"], self.update)

               	self.show()
        #
        #check for new data and redraw the listing, is called by timer
        #
        def update(self):
		global global_pwmanager

                global_pwmanager.update()
                pwListPanel.draw()

	def ignoreEvent(self,event):
		pass	
        
#
#       the panel for deleting passwords
#
class PasswordDetailsDeletePanel(gtk.ScrolledWindow):
        #
        #       the constructor
        #
        def __init__(self, parent, what):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		box = gtk.VBox()
                button = gtk.Button(label="delete entry")
                button.connect("clicked",self.delete,what)
		box.pack_start(button,expand=False,padding=10)

		self.__container = gtk.HBox()
		self.__container.pack_start(box,expand=False,padding=8)

		self.add_with_viewport(self.__container)

		self.show_all()
        
        #
        #       create the function for deleting a password
        #
	def delete(self,widget,what):
		#for idiots
		confirmation = gtk.MessageDialog(buttons=gtk.BUTTONS_OK_CANCEL)
		confirmation.set_markup("Sind Sie sicher, dass dieses Objekt loeschen wollen?")
		confirmation.format_secondary_text("Sicher?")
		result = confirmation.run()
		confirmation.destroy()
		if result == gtk.RESPONSE_OK:
			#delete item
			global global_pwmanager
			global_pwmanager.deleteItem(what["realpath"],what["name"],list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])))

			#try send messages
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw windows
			pwDetailPanel.draw(what)
			pwListPanel.draw()
	
#
#       the panel for listing the passwords of a directory
#
class PasswordDetailsListPanel(gtk.ScrolledWindow):
        #
        #       the constructor
        #
        def __init__(self, parent, what):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.__container = gtk.HBox()

                self.draw(what)
		self.show_all()

        #
        #       (re)draw the panel
        #
        def draw(self,what):
		global glob_pwmanager

		#clear canvas
		self.__container.destroy()

		box = gtk.VBox()

		#action only allowed for directories
                if what["type"] == "dir":
                        #print list of passwords contained in the directory and its subdirectories
                        count = 0
                        for pw in global_pwmanager.getAllPwsFromArray(what["content"]):
                                text = gtk.Label(str= pw["name"]+" "+pw["username"]+":"+pw["password"]+" "+pw["host"]+" --"+pw["comment"])
				text.set_selectable(True)

				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)
                else:
                        text = gtk.Label(str="Error: Type unknown")
			box.pack_start(text,expand=False,padding=3)

		self.__container = gtk.HBox()
		self.__container.pack_start(box,expand=False,padding=8)

		self.add_with_viewport(self.__container)
#
#       the panel for displaying a lock
#
class LockDetailsPanel(gtk.ScrolledWindow):
        #
        #       the constructor
        #
	def __init__(self, parent, what):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.__container = gtk.HBox()

		self.draw(what)
		self.show_all()
	
	#
	#	create the function for opening a lock
	#
	def unlock(self,widget,event,what):
		#unlock the password
		global global_pwmanager
		global_pwmanager.unlock(what["realpath"],what["name"],what["lock"],list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])))

		#try send messages
		try:
			global_pwmanager.update()
		except:
			pass

		#redraw yourself
		self.draw(what)
		
	#
        #       (re)draw the panel
        #
	def draw(self,what):
		#clear canvas
		self.__container.destroy()

		self.__container = gtk.HBox()
		box = gtk.VBox()

		#print Information about the lock
                text = gtk.Label(str="Anzahl der Locks die noch geloest werden muessen: "+str(what["lock"]["level"]+1))
		box.pack_start(text,expand=False,padding=10)

		#print the partner allowed to open the lock
		for name in what["lock"]["partners"]:

			fancyName = name
			
			#red color for partners wich opened the lock already
			if name in what["lock"]["unlocked"]:
				fancyName = '<span foreground="red">'+name+'</span>'
				
			#black text for yourself if you haven't opened the lock
			if name == global_pwmanager.glob_userid and ((not what["lock"].has_key("messages")) or what["lock"]["messages"] != []):
				fancyName = '<span foreground="black">'+name+'</span>'

			text = gtk.Label(name)
			text.set_markup(fancyName)
			text.show()
			
			box.pack_start(text,expand=False,padding=6)
				
		#the button for unlocking the lock
		if (not what["lock"].has_key("messages")) or what["lock"]["messages"] != []:
                        button = gtk.Button(label="unlock")
                        button.Bind("cklicked", self.unlock, what)
			box.pack_start(text,expand=False,padding=6)

		self.__container = gtk.HBox()
		self.__container.pack_start(box,expand=False,padding=8)

		self.add_with_viewport(self.__container)

#
#       the panel for creating new passwords and directories
#
class PasswordDetailsNewPanel(gtk.ScrolledWindow):

        #
        #       the constructor
        #
        def __init__(self, parent, what):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.__container = gtk.HBox()

		#set thetoggle defaults
		self.toggle = {}
		self.toggle["dir"] = 1
		self.toggle["pw"] = 1
		self.toggle["key"] = 0
		self.toggle["lock"] = 0

		#draw panel
		self.draw(what)

	#
	#	toggle the keys
	#
	def toggleKey(self,widget,what):
		if (self.toggle["key"]):
			self.toggle["key"] = 0
		else:
			self.toggle["key"] = 1
		self.draw(what)

	#
	#	toggle the directories
	#
	def toggleDir(self,widget,what):
		if (self.toggle["dir"]):
			self.toggle["dir"] = 0
		else:
			self.toggle["dir"] = 1
		self.draw(what)

	#
	#	toggle the passwords
	#
	def togglePw(self,widget,what):
		if (self.toggle["pw"]):
			self.toggle["pw"] = 0
		else:
			self.toggle["pw"] = 1
		self.draw(what)

	#
	#	toggle the lock
	#
	def toggleLock(self,widget,what):
		if (self.toggle["lock"]):
			self.toggle["lock"] = 0
		else:
			self.toggle["lock"] = 1
		self.draw(what)

        #
        #       the function for (re)drawing the panel
        #
        def draw(self,what):
		#remove old stuff
		self.__container.destroy()

		box = gtk.VBox()

		#the contents for directories
                if what["type"] == "dir":
			count = 0
			
                        #the toggle for the new directories form
			subBox = gtk.HBox()
			if (self.toggle["dir"]):
				sign = "-"
			else:
				sign = "+"
			button = gtk.Button(label=sign)
			button.set_size_request(20,20)
			button.connect("clicked",self.toggleDir,what)
			subBox.pack_start(button,expand=False,padding=2)
                        text = gtk.Label(str="create new directory")
			subBox.pack_start(text,expand=False,padding=4)
			box.pack_start(subBox,expand=False,padding=6)

			#toggle the form
			if (self.toggle["dir"]):
				text = gtk.Label(str="name:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				name = gtk.Entry()
				name.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(name,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				button = gtk.Button(label="create")
				button.connect("clicked",self.saveDir,what,name)
				wrapper = gtk.HBox()
				wrapper.pack_start(button,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=8)

			#the toggle for new pws form
			subBox = gtk.HBox()
			if (self.toggle["pw"]):
				sign = "-"
			else:
				sign = "+"
			button = gtk.Button(label=sign)
			button.set_size_request(20,20)
			button.connect("clicked",self.togglePw,what)
			subBox.pack_start(button,expand=False,padding=2)
			text = gtk.Label(str="create new password:")
			subBox.pack_start(text,expand=False,padding=4)
			box.pack_start(subBox,expand=False,padding=6)

			#toggle the form
			if (self.toggle["pw"]):
				text = gtk.Label(str="name:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				name = gtk.Entry()
				name.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(name,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="username:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				username = gtk.Entry()
				username.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(username,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="password:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				password = gtk.Entry()
				password.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(password,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)
				
				text = gtk.Label(str="host:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				host = gtk.Entry()
				host.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(host,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="comment:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				comment = gtk.Entry()
				comment.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(comment,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)
				
				save = gtk.Button(label="create")
				save.connect("clicked",self.savePw,what,username,password,host,comment,name)
				wrapper = gtk.HBox()
				wrapper.pack_start(save,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=8)

			#the toggle for new locks form
			subBox = gtk.HBox()
			if (self.toggle["key"]):
				sign = "-"
			else:
				sign = "+"
			button = gtk.Button(label=sign)
			button.set_size_request(20,20)
			button.connect("clicked",self.toggleKey,what)
			subBox.pack_start(button,expand=False,padding=2)
			text = gtk.Label(str="create new key:")
			subBox.pack_start(text,expand=False,padding=4)
			box.pack_start(subBox,expand=False,padding=6)

			if (self.toggle["key"]):

				#the form for editing passwords

				text = gtk.Label(str="name:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				name = gtk.Entry()
				name.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(name,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)

				text = gtk.Label(str="password:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				password = gtk.Entry()
				password.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(password,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="keyid:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				keyid = gtk.Entry()
				keyid.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(keyid,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="host:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				host = gtk.Entry()
				host.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(host,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="key:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				keyField = gtk.TextView()
				keyField.set_size_request(400,200)
				key = gtk.TextBuffer()
				keyField.set_buffer(key)
				box.pack_start(keyField,expand=False,padding=3)
				wrapper = gtk.HBox()
				wrapper.pack_start(keyField,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="comment:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				comment = gtk.Entry()
				comment.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(comment,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				save = gtk.Button(label="save")
				save.connect("clicked",self.saveExternalKey,what,name,key,keyid,password,host,comment)
				wrapper = gtk.HBox()
				wrapper.pack_start(save,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=8)

			#the toggle for new locks form
			subBox = gtk.HBox()
			if (self.toggle["lock"]):
				sign = "-"
			else:
				sign = "+"
			button = gtk.Button(label=sign)
			button.set_size_request(20,20)
			button.connect("clicked",self.toggleLock,what)
			subBox.pack_start(button,expand=False,padding=2)
			text = gtk.Label(str="create new lock:")
			subBox.pack_start(text,expand=False,padding=4)
			box.pack_start(subBox,expand=False,padding=6)

			#toggle the form
			if (self.toggle["lock"]):
				text = gtk.Label(str="Partner mit Berechtigung lock zu loesen:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				partner = gtk.Entry()
				partner.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(partner,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str= "Anzahl der Partner die benoetigt werden, um lock zu loesen:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				level = gtk.Entry()
				level.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(level,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="name:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				name = gtk.Entry()
				name.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(name,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="username:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				username = gtk.Entry()
				username.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(username,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="password:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				password = gtk.Entry()
				password.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(password,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="host:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				host = gtk.Entry()
				host.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(host,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				text = gtk.Label(str="comment:")
				wrapper = gtk.HBox()
				wrapper.pack_start(text,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=5)
				comment = gtk.Entry()
				comment.set_size_request(400,20)
				wrapper = gtk.HBox()
				wrapper.pack_start(comment,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

				save = gtk.Button(label="create")
				save.connect("clicked",self.saveLock,what,username,password,host,comment,name,partner,level)
				wrapper = gtk.HBox()
				wrapper.pack_start(save,expand=False,padding=10)
				box.pack_start(wrapper,expand=False,padding=3)

                #the content for passwords
                elif what["type"] == "pw":

			#the form for editing passwords
                        text = gtk.Label(str="username:")
			box.pack_start(text,expand=False,padding=10)
			username = gtk.Entry()
			username.set_size_request(400,20)
			username.set_text(what["username"])
			box.pack_start(username,expand=False,padding=3)

		        text = gtk.Label(str="password:")
			box.pack_start(text,expand=False,padding=3)
			password = gtk.Entry()
			password.set_size_request(400,20)
			password.set_text(what["password"])
			box.pack_start(password,expand=False,padding=3)

                        text = gtk.Label(str="host:")
			box.pack_start(text,expand=False,padding=3)
			host = gtk.Entry()
			host.set_size_request(400,20)
			host.set_text(what["host"])
			box.pack_start(host,expand=False,padding=3)

                        text = gtk.Label(str="comment:")
			box.pack_start(text,expand=False,padding=3)
			comment = gtk.Entry()
			comment.set_size_request(400,20)
			comment.set_text(what["comment"])
			box.pack_start(comment,expand=False,padding=3)

                        save = gtk.Button(label="save")
                        save.connect("clicked",self.changePw,what,username,password,host,comment)
			box.pack_start(save,expand=False,padding=3)

		elif what["type"] == "key":

			#the form for editing passwords
                        text = gtk.Label(str="password:")
			box.pack_start(text,expand=False,padding=10)
			password = gtk.Entry()
			password.set_size_request(400,20)
			password.set_text(what["password"])
			box.pack_start(password,expand=False,padding=3)

		        text = gtk.Label(str="keyid:")
			box.pack_start(text,expand=False,padding=3)
			keyid = gtk.Entry()
			keyid.set_size_request(400,20)
			keyid.set_text(what["keyid"])
			box.pack_start(keyid,expand=False,padding=3)

                        text = gtk.Label(str="host:")
			box.pack_start(text,expand=False,padding=3)
			host = gtk.Entry()
			host.set_size_request(400,20)
			host.set_text(what["host"])
			box.pack_start(host,expand=False,padding=3)

                        text = gtk.Label(str="key:")
			box.pack_start(text,expand=False,padding=3)
			keyField = gtk.TextView()
			keyField.set_size_request(400,200)
			key = gtk.TextBuffer()
			keyField.set_buffer(key)
			key.set_text(what["key"])
			box.pack_start(keyField,expand=False,padding=3)

                        text = gtk.Label(str="comment:")
			box.pack_start(text,expand=False,padding=3)
			comment = gtk.Entry()
			comment.set_size_request(400,20)
			comment.set_text(what["comment"])
			box.pack_start(comment,expand=False,padding=3)

                        save = gtk.Button(label="save")
                        save.connect("clicked",self.changeExternalKey,what,key,keyid,password,host,comment)
			box.pack_start(save,expand=False,padding=3)

		#only valid for directories
                else:
                        text = gtk.Label(str="Error: Type unknown")
			box.pack_start(text,expand=False,padding=3)

		self.__container = gtk.HBox()
		self.__container.pack_start(box,expand=False,padding=8)

		self.add_with_viewport(self.__container)
		self.show_all()

	def saveExternalKey(self,widget,what,name,key,keyid,password,host,comment):
		global global_pwmanager

		#do not allow passwords with empty names
		if (name.get_text()!=""):
			#save key
			tmp = copy.copy(what["realpath"])
			tmp.append(what["name"])
			global_pwmanager.saveExternalKey(list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])),tmp,password.get_text(),name.get_text(),key.get_text(key.get_start_iter(),key.get_end_iter()),keyid.get_text(),host.get_text(),comment.get_text())

			#try to update
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw panels
			pwDetailPanel.draw(what)
			pwListPanel.draw()

        #
        #       create the function for saving a password
        #
        def savePw(self,widget,what,username,password,host,comment,name):
		global global_pwmanager

		#do not allow passwords with empty names
		if (name.get_text()!=""):
			#save password
			tmp = copy.copy(what["realpath"])
			tmp.append(what["name"])
			global_pwmanager.savePassword(password.get_text(),username.get_text(),list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])),tmp,name.get_text(),host.get_text(),comment.get_text())

			#try to update
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw panels
			pwDetailPanel.draw(what)
			pwListPanel.draw()

        #
        #       create the function for saving a password
        #
	def saveLock(self,widget,what,username,password,host,comment,name,partner,num):
		global global_pwmanager

		#TODO: proper partner selection
		import json
		partnerArr = json.loads(partner.get_text())

		level = int(num.get_text())
		#do not allow empty names or useless number of partners needed to unlock the lock
		if (name.get_text()!="" and level<=len(partnerArr) and level>1):

			tmp = copy.copy(what["realpath"])
			tmp.append(what["name"])

			#add shares for the partners needed to open the lock
			for item in partnerArr:
				global_pwmanager.addPartner(list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])),item)
				global_pwmanager.addKey(name.get_text(),tmp,list(set(what["share"]+what["inheritedShare"]+partnerArr)),item)
			
			#save the lock
			global_pwmanager.saveLock(password.get_text(),username.get_text(),list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"]+partnerArr)),tmp,name.get_text(),host.get_text(),comment.get_text(),partnerArr,level)

			#add shares for the lock
			for item in partnerArr:
				global_pwmanager.addKey(name.get_text(),tmp,list(set(what["share"]+what["inheritedShare"]+partnerArr)),item)

			#try to send messages
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw panels
			pwDetailPanel.draw(what)
			pwListPanel.draw()

	#
	#	change the stored key
	#
	def changeExternalKey(self,widget,what,key,keyid,password,host,comment):
		global global_pwmanager

		global_pwmanager.saveExternalKey(list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])),what["realpath"],password.get_text(),what["name"],key.get_text(key.get_start_iter(),key.get_end_iter()),keyid.get_text(),host.get_text(),comment.get_text(),auth="")

		#try to update
		try:
			global_pwmanager.update()
		except:
			pass

		#redraw the panels
		pwDetailPanel.draw(what)
		pwListPanel.draw()

        #
        #       create the function for changing a password
        #
        def changePw(self,widget,what,username,password,host,comment):
		global global_pwmanager

		global_pwmanager.savePassword(password.get_text(),username.get_text(),list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])),what["realpath"],what["name"],host.get_text(),comment.get_text())

		#try to update
		try:
			global_pwmanager.update()
		except:
			pass

		#redraw the panels
		pwDetailPanel.draw(what)
		pwListPanel.draw()

        #
        #       create the function for saving a directory
        #
	def saveDir(self,widget,what,name):

		#empty names are not allowed
		if (name.get_text()!=""):

			#save directory
			tmp = copy.copy(what["realpath"])
			tmp.append(what["name"])
			global_pwmanager.saveDirectory(name.get_text(),tmp,list(set(what["share"]+what["inheritedShare"]+what["roShare"]+what["inheritedRoShare"])))

			#try to update
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw panels
			pwDetailPanel.draw(what)
			pwListPanel.draw()

#
#       the panel for listing and editing the shares
#
class PasswordDetailsSharePanel(gtk.ScrolledWindow):
        #
        #       the constructor
        #
        def __init__(self, parent, what):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.__container = gtk.HBox()
                self.draw(what)

        #
        #       the function for (re)drawing the panel
        #
        def draw(self,what):
                #clear page
		self.__container.destroy()

		box = gtk.VBox()

		#list the normal shares
		if len(what["share"]):
			text = gtk.Label(str="shares:")
			wrapper = gtk.HBox()
			wrapper.pack_start(text,expand=False,padding=6)
			box.pack_start(wrapper,expand=False,padding=10)

			subBox = gtk.HBox()
			for item in what["share"]:

				#get the nice name for shares
				sharename = item
				if (item in global_pwmanager.glob_data["users"].keys()):
					sharename = global_pwmanager.glob_data["users"][item]["name"]

				#the button for removing a normal share
				button = gtk.Button(label="-")
				button.connect("clicked",self.deleteKey,what,item)
				subBox.pack_start(button,expand=False,padding=8)

				#print the sharename
				text = gtk.Label(str=sharename)
				subBox.pack_start(text,expand=False,padding=0)
			box.pack_start(subBox,expand=False,padding=6)

		#list the inherited shares
		if len(what["inheritedShare"]):
			text = gtk.Label(str="inherited shares:")
			wrapper = gtk.HBox()
			wrapper.pack_start(text,expand=False,padding=6)
			box.pack_start(wrapper,expand=False,padding=6)

			for item in what["inheritedShare"]:


				#get the nice name for shares
				sharename = item
				if (item in global_pwmanager.glob_data["users"].keys()):
					sharename = global_pwmanager.glob_data["users"][item]["name"]


				#mark the point where inherited shares come with red color
				fancySharename = sharename
				if item in what["foreignInheritedShare"]:
					fancySharename = '<span foreground="red">'+sharename+'</span>'

				#print sharename
				text = gtk.Label(str=sharename)
				text.set_markup(fancySharename)

				subBox = gtk.HBox()
				box.pack_start(subBox,expand=False,padding=6)
				subBox.pack_start(text,expand=False,padding=30)

		#list the read-only shares
		if len(what["roShare"]):
			text = gtk.Label(str="readonly shares:")
			wrapper = gtk.HBox()
			wrapper.pack_start(text,expand=False,padding=6)
			box.pack_start(wrapper,expand=False,padding=6)

			subBox = gtk.HBox()
			for item in what["roShare"]:
				
				#get the nice name for shares
				sharename = item
				if (item in global_pwmanager.glob_data["users"].keys()):
					sharename = global_pwmanager.glob_data["users"][item]["name"]

				#the button for removing the share
				button = gtk.Button(label="-")
				button.connect("clicked",self.deleteRoKey,what,item)
				subBox.pack_start(button,expand=False,padding=8)

				#print the sharename
				text = gtk.Label(str=sharename)
				subBox.pack_start(text,expand=False,padding=0)
			box.pack_start(subBox,expand=False,padding=8)

		#list the inherited read-only shares
		if len(what["inheritedRoShare"]):
			#list the ro shares
			text = gtk.Label(str="inherited readonly shares:")
			wrapper = gtk.HBox()
			wrapper.pack_start(text,expand=False,padding=6)
			box.pack_start(wrapper,expand=False,padding=6)

			subBox = gtk.HBox()
			for item in what["inheritedRoShare"]:

				#get the nice name for shares
				sharename = item
				if (item in global_pwmanager.glob_data["users"].keys()):
					sharename = global_pwmanager.glob_data["users"][item]["name"]


				#mark the point where inherited shares come with red color
				fancySharename = sharename
				if item in what["foreignInheritedRoShare"]:
					fancySharename = '<span foreground="red">'+sharename+'</span>'

				#print sharename
				text = gtk.Label(str=sharename)
				text.set_markup(fancySharename)
				subBox.pack_start(text,expand=False,padding=30)
			box.pack_start(subBox,expand=False,padding=8)
                
                #the formular for minipulating shares
		choices = []
		for item in global_pwmanager.glob_data["users"].keys():
			choices.append(global_pwmanager.glob_data["users"][item]["name"])
                text = gtk.Label(str="Partnername:")
		wrapper = gtk.HBox()
		wrapper.pack_start(text,expand=False,padding=6)
		box.pack_start(wrapper,expand=False,padding=6)
		keyid = gtk.combo_box_new_text()
		for id in choices:
			keyid.append_text(id)
		wrapper = gtk.HBox()
		wrapper.pack_start(keyid,expand=False,padding=6)
		box.pack_start(wrapper,expand=False,padding=6)
                buttonShare = gtk.Button(label="add new share")
                buttonShare.connect("clicked",self.addKey,what,keyid)
		wrapper = gtk.HBox()
		wrapper.pack_start(buttonShare,expand=False,padding=6)
		box.pack_start(wrapper,expand=False,padding=6)
                buttonRoShare = gtk.Button(label="add new readonly share")
                buttonRoShare.connect("clicked",self.addRoKey,what,keyid)
		wrapper = gtk.HBox()
		wrapper.pack_start(buttonRoShare,expand=False,padding=6)
		box.pack_start(wrapper,expand=False,padding=6)

		self.__container = gtk.HBox()
		self.__container.pack_start(box,expand=False,padding=8)

		self.add_with_viewport(self.__container)
		self.show_all()


        #
        #      	(help)
	#	adds read-only access for a key to the directory and its subdirectories
        #
        def addRoKeysRecursive(self,what,keyid,auth):
		global global_pwmanager

                for item in what["content"]:
                        
			#TODO:add Locks
                        #save entries and loop further
                        if item["type"] == "dir":
				#create the directory for the new user
                                global_pwmanager.saveDirectory(item["name"],item["realpath"],[keyid],auth=auth)

				#call yourself for this directory
                                self.addRoKeysRecursive(item,keyid,auth)

				#add new partner
                                for key in item["share"]:
                                        global_pwmanager.addPartner([keyid],key)
					global_pwmanager.addPartner(item["share"],key)
                        elif item["type"] == "pw":
				#create the password for the new user
                                global_pwmanager.savePassword(item["password"],item["username"],[keyid],item["realpath"],item["name"],item["host"],item["comment"],auth=auth)

                        #add the keys
                        for itemShare in item["share"]:
                              global_pwmanager.addInheritedRoKey(item["name"],item["realpath"],[itemShare],keyid)

	#
	#	create the function for giving read-only access to a keyid
	#
	def addRoKey(self,widget,what,name):
		global global_pwmanager

		if (name.get_active_text()!=""):
			#get the authentication
			auth = global_pwmanager.getAuth(what)
			keyid = ""
			for item in global_pwmanager.glob_data["users"].keys():
				if global_pwmanager.glob_data["users"][item]["name"] == name.get_active_text():
					keyid = item

			if keyid == "":
				print("user not found")
				return

			#add new partner
			for item in what["share"]:
				global_pwmanager.addPartner([keyid],item)
				global_pwmanager.addKey(what["name"],what["realpath"],[keyid],item)
			for item in what["inheritedShare"]:
				global_pwmanager.addPartner([keyid],item)
				global_pwmanager.addInheritedKey(what["name"],what["realpath"],[keyid],item)
			global_pwmanager.addPartner(what["share"]+what["inheritedShare"],keyid)

			#build the directory
			if what["type"] == "dir":
				global_pwmanager.saveDirectory(what["name"],what["realpath"],[keyid],auth=auth)
			else:
				global_pwmanager.savePassword(what["password"],what["username"],[keyid],what["realpath"],what["name"],what["host"],what["comment"],auth=auth)

			#add the permissions
			global_pwmanager.addRoKey(what["name"],what["realpath"],list(set(what["inheritedShare"]+what["share"])),keyid)
		
			#handle multiple share level
			if what["type"] == "dir":
				self.addRoKeysRecursive(what,keyid,auth)
			
			#update the application
			try:
				global_pwmanager.update()
			except:
				pass
			pwDetailPanel.draw(what)
			pwListPanel.draw()

        #
        #      	(help)
	#	adds access for a key to the directory and its subdirectories
        #
        def addKeysRecursive(self,what,keyid,auth):
		global global_pwmanager

		#loop through the directory
                for item in what["content"]:
                        
			#TODO: Add Lock
                        #save entries and loop further
                        if item["type"] == "dir":
                                global_pwmanager.saveDirectory(item["name"],item["realpath"],[keyid],auth=auth)
                                self.addKeysRecursive(item,keyid,auth)

				#add new partner
                                for key in item["share"]:
                                        global_pwnanager.addPartner([keyid],key)
                        elif item["type"] == "pw":
                                global_pwmanager.savePassword(item["password"],item["username"],[keyid],item["realpath"],item["name"],item["host"],item["comment"],auth=auth)

                        #add the keys
                        for itemShare in item["share"]:
                                global_pwmanager.addKey(item["name"],item["realpath"],[keyid],itemShare,auth=auth)
                                global_pwmanager.addInheritedKey(item["name"],item["realpath"],[itemShare],keyid)

				#add new partner
                                for key in item["share"]:
                                        global_pwmanager.addPartner([keyid],key)

        #
        #       create the function for adding a key
        #
        def addKey(self,widget,what,name):
		global global_pwmanager

		if (name.get_active_text()!=""):
			#get the authentication token
			auth = global_pwmanager.getAuth(what)

			#get keyid
			keyid = ""
			for item in global_pwmanager.glob_data["users"].keys():
				if global_pwmanager.glob_data["users"][item]["name"] == name.get_active_text():
					keyid = item
			if keyid == "":
				print("user not found")
				return

			#add new partner
			for item in (what["inheritedShare"]+what["share"]):
				global_pwmanager.addPartner([keyid],item)

			#save data contained in the directory
			#TODO: save Lock
			if what["type"] == "dir":
				global_pwmanager.saveDirectory(what["name"],what["realpath"],[keyid],auth=auth)
			else:
				global_pwmanager.savePassword(what["password"],what["username"],[keyid],what["realpath"],what["name"],what["host"],what["comment"],auth=auth)

			#add the permissions
			global_pwmanager.addKey(what["name"],what["realpath"],[keyid],keyid,auth=auth)
			global_pwmanager.addKey(what["name"],what["realpath"],list(set(what["inheritedShare"]+what["share"])),keyid)
			for item in what["roShare"]:
				global_pwmanager.addRoKey(what["name"],what["realpath"],[keyid],item)
			for item in what["inheritedRoShare"]:
				global_pwmanager.addInheritedRoKey(what["name"],what["realpath"],[keyid],item)
			for item in what["share"]:
				global_pwmanager.addKey(what["name"],what["realpath"],item,keyid)
			for item in what["inheritedShare"]:
				global_pwmanager.addInheritedKey(what["name"],what["realpath"],item,keyid)
		
			#call yourself to step down into the next directory
			if what["type"] == "dir":
				self.addKeysRecursive(what,keyid,auth)

			#set the permissiion on the added user
			for item in what["inheritedShare"]:
				global_pwmanager.addInheritedKey(what["name"],what["realpath"],[keyid],item,auth=auth)
			for item in what["share"]:
				global_pwmanager.addKey(what["name"],what["realpath"],[keyid],item,auth=auth)
			
			#update the application
			try:
				global_pwmanager.update()
			except:
				pass

			#redraw the panels
			pwDetailPanel.draw(what)
			pwListPanel.draw()

	#
	#	(help)
	#	remove read-only access for a key in a directory and its content
	#
        def deleteRoKeysRecursive(self,what,keyid):
		global global_pwmanager

		#loop through the directory
                noDeleteFlag = 0
                for item in what["content"]:
                        if not keyid in item["roShare"]+item["foreignInheritedRoShare"]:
                                #remove the keys lower down the tree
                                for keyid in item["share"]+item["foreignInheritedShare"]:
					#TODO: why is this commented out?
                                        #comm.deleteRoInheritedKey(item["name"],item["realpath"],[share],keyid)
					pass

				#do not delete if the key has other read access
                                localNoDeleteFlag = 0
				if keyid in item["share"]+item["inheritedShare"]:
					noDeleteFlag = 1
					localNoDeleteFlag = 1

                                #delete entries and loop further
                                if item["type"] == "dir":
                                        localNoDeleteFlag += self.deleteRoKeysRecursive(item,keyid)
                                        noDeleteFlag = noDeleteFlag + localNoDeleteFlag
                                if not localNoDeleteFlag:
                                        global_pwmanager.deleteItem(item["realpath"],item["name"],[keyid])
                        else:
                                noDeleteFlag = 1
                return noDeleteFlag

	#
	#	create the function for removing read-only access for a keyid
	#
        def deleteRoKey(self,widget,what,keyid):
		global global_pwmanager

		noDeleteFlag = 0
		#handle multiple share level
		if not keyid in what["inheritedShare"]+what["share"]:
			if what["type"] == "dir":
				noDeleteFlag = self.deleteRoKeysRecursive(what,keyid)
		else:
			noDeleteFlag = 1

		#remove key and delete item
		if not noDeleteFlag:
			global_pwmanager.deleteItem(what["realpath"],what["name"],[keyid])
		global_pwmanager.deleteRoKey(what["name"],what["realpath"],list(set(what["inheritedShare"]+what["share"])),keyid)
	
		#update the application
		try:
			global_pwmanager.update()
		except:
			pass
		pwDetailPanel.draw(what)
		pwListPanel.draw()

	#
	#	(help)
	#	remove access for a keyid from a directory and its content
	#	
        def deleteKeysRecursive(self,what,keyid):
		global global_pwmanager

		#loop through the directory
                noDeleteFlag = 0
                for item in what["content"]:
                        if not keyid in item["share"]:
                                #remove the keys lower down the tree
                                for global_pwmanager in item["share"]:
                                        global_pwmanager.deleteKey(item["name"],item["realpath"],[keyid],global_pwmanager)
                                        global_pwnanager.deleteInheritedKey(item["name"],item["realpath"],[global_pwmanager],keyid)

                                #delete entries and loop further
                                localNoDeleteFlag = 0
                                if item["type"] == "dir":
                                        localNoDeleteFlag = self.deleteKeysRecursive(item,keyid)
                                        noDeleteFlag = noDeleteFlag + localNoDeleteFlag
                                if not localNoDeleteFlag:
                                        global_pwmanager.deleteItem(item["realpath"],item["name"],[keyid])
                        else:
                                noDeleteFlag = 1
                return noDeleteFlag

        #
        #       create the function for deleting a key
        #
        def deleteKey(self,widget,what,keyid):
		global global_pwmanager

		noDeleteFlag = 0
		#handle multiple share level
		if not keyid in what["inheritedShare"]:
			if what["type"] == "dir":
				noDeleteFlag = self.deleteKeysRecursive(what,keyid)
		else:
			noDeleteFlag = 1
		for inShare in what["inheritedShare"]:
			if inShare != keyid:
				global_pwmanager.deleteInheritedKey(what["name"],what["realpath"],[keyid],inShare)

		#remove keys and delete item
		if not noDeleteFlag:
			global_pwmanager.deleteItem(what["realpath"],what["name"],[keyid])
		global_pwmanager.deleteKey(what["name"],what["realpath"],[keyid],keyid)
		global_pwmanager.deleteKey(what["name"],what["realpath"],list(set(what["inheritedShare"]+what["share"])),keyid)
	
		#update the application
		try:
			global_pwmanager.update()
		except:
			pass

		#redraw the panels
		pwDetailPanel.draw(what)
		pwListPanel.draw()

#
#       the panel for showing the details of a password or directory
#
class PasswordDetailsPanel(gtk.ScrolledWindow):
        #
        #       the constructor
        #
        def __init__(self, parent):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.notebook = gtk.Notebook()

        #
        #       create the content of the panel
        #
        def display(self,what):
		self.notebook.destroy()

		self.notebook = gtk.Notebook()
		self.notebook.show()
		self.notebook.set_tab_pos(gtk.POS_TOP)
		self.notebook.set_scrollable(True)
		self.add_with_viewport(self.notebook)

                # create some nested tabs on the first tab
                self.listTab = PasswordDetailsListPanel(self,what)
                self.listTab.show()
                self.newTab = PasswordDetailsNewPanel(self,what)
                self.newTab.show()
		self.deleteTab = PasswordDetailsDeletePanel(self,what)
		self.deleteTab.show()
                self.shareTab = PasswordDetailsSharePanel(self,what)
                self.shareTab.show()
		if what["type"] == "lock":
			self.lockTab = LockDetailsPanel(self,what)
			self.lockTab.show()
			self.notebook.append_page(self.lockTab,tab_label=gtk.Label(str="lock"))
			self.notebook.append_page(self.deleteTab,tab_label=gtk.Label(str="delete"))
			self.notebook.append_page(self.shareTab,tab_label=gtk.Label(str="share"))
		else:
			if (what["type"]=="dir"):
				self.notebook.append_page(self.listTab,tab_label=gtk.Label(str="pwListe"))
			if global_pwmanager.glob_userid in what["inheritedShare"]+what["share"]:
				if what["type"]=="dir":
					self.notebook.append_page(self.newTab,tab_label=gtk.Label(str="new"))
				else:
					self.notebook.append_page(self.newTab,tab_label=gtk.Label(str="edit"))
			if global_pwmanager.glob_userid in what["inheritedShare"]+what["share"] and len(what["realpath"]) != 0:
				self.notebook.append_page(self.deleteTab,tab_label=gtk.Label(str="delete"))
			if global_pwmanager.glob_userid in what["inheritedShare"]+what["share"] and len(what["realpath"]) != 0:
				self.notebook.append_page(self.shareTab,tab_label=gtk.Label(str="share"))

        #
        #       redraw the content of this panel
        #
        def draw(self,what):
                self.listTab.draw(what)
                self.newTab.draw(what)
                self.shareTab.draw(what)
                pass

#
#       the panel for listing the existing passwords and directories without details
#
class PasswordListPanel(gtk.ScrolledWindow):

        #
        #       the constructor
        #
        def __init__(self, parent):
		gtk.ScrolledWindow.__init__(self)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.tooltips = gtk.Tooltips()

		self.__content	= gtk.VBox()

                self.__parent	= parent

		self.__wrapper = gtk.EventBox()
		self.__wrapper.add(self.__content)
		color = gtk.gdk.color_parse('#ffffff')
		self.__wrapper.modify_bg(gtk.STATE_NORMAL, color)
		self.__wrapper.show()

		self.draw()
		self.add_with_viewport(self.__wrapper)

        #
        #       the function for printing directories and the contained passwords recursive
        #
        def printDir(self,level,sizer,rootDir):
		global global_pwmanager

                #print directories recursive
                for currentDir in global_pwmanager.getDirsFromArray(rootDir):
                        #display the folder, replace Name with short on first level 
                        if (level == 0):
				caption = global_pwmanager.glob_data["users"][currentDir["name"]]["name"]
                        else:
                                caption = currentDir["name"]

                        #set the warnig colour according to share
                        numShares = len(set(currentDir["share"]+currentDir["inheritedShare"]+currentDir["roShare"]+currentDir["inheritedRoShare"]))
                        if numShares==1:
				fancyCaption = '<span foreground="#009C00">'+caption+'</span>'
                        elif numShares<global_pwmanager.glob_config["warnlevel"]:
				fancyCaption = '<span foreground="#FF7E00">'+caption+'</span>'
                        else:
				fancyCaption = '<span foreground="#EE0000">'+caption+'</span>'

                        #set nonchangable items to grey
                        if not global_pwmanager.glob_userid in currentDir["share"]+currentDir["inheritedShare"]:
				fancyCaption = '<span foreground="#C8C8C8">'+caption+'</span>'

			#highlight the active folder
			if "active" in currentDir and currentDir["active"]:
				fancyCaption = '<span foreground="#000000" background="blue">'+caption+'</span>'

			#display the name
			text = gtk.Label(caption)
			text.set_markup(fancyCaption)
			text.show()
			
			wrapper = gtk.EventBox()
			wrapper.add(text)
			color = gtk.gdk.color_parse('#ffffff')
			wrapper.modify_bg(gtk.STATE_NORMAL, color)
			wrapper.show()
			wrapper.connect('button_press_event', self.buttonPressEvent, currentDir) 
			
                        #set the buttons
                        if currentDir["open"]:
				dirSubsizer = gtk.HBox()
				dirSubsizer.show()

				button = gtk.Button(label='-')
				button.show()
  				self.tooltips.set_tip(button,"click to hide")
				recursiveSizer = gtk.VBox()
				recursiveSizer.show()
                                self.printDir(level+1,recursiveSizer,currentDir["content"])
				dirSubsizer.pack_start(recursiveSizer,expand=False,padding=30)
                        else:
				button = gtk.Button(label='+')
				button.show()
  				self.tooltips.set_tip(button,"click to show")
				dirSubsizer = None
 			button.connect("clicked",self.showHide,currentDir)
 			button.set_size_request(20,20)

			headSubsizer = gtk.HBox()
			headSubsizer.show()
			headSubsizer.pack_start(button,expand=False,padding=2)
			headSubsizer.pack_start(wrapper,expand=False,padding=2)
			sizer.pack_start(headSubsizer,expand=False,padding=2)

			if dirSubsizer:
				sizer.pack_start(dirSubsizer,expand=False,padding=2)


                #print passwords
                for pw in global_pwmanager.getPwsFromArray(rootDir):
			
			caption = pw["name"]

                        #set the text colour
                        numShares = len(set(pw["share"]+pw["inheritedShare"]+pw["roShare"]+pw["inheritedRoShare"]))
                        if numShares==1:
				fancyCaption = '<span foreground="#009C00">'+caption+'</span>'
                        elif numShares<global_pwmanager.glob_config["warnlevel"]:
				fancyCaption = '<span foreground="#FF7E00">'+caption+'</span>'
                        else:
				fancyCaption = '<span foreground="#EE0000">'+caption+'</span>'

                        #set nonchangable items to grey
                        if not global_pwmanager.glob_userid in pw["share"]+pw["inheritedShare"]:
				fancyCaption = '<span foreground="#C8C8C8">'+caption+'</span>'

			#highlight the active folder
			if "active" in pw and pw["active"]:
				fancyCaption = '<span foreground="#000000" background="blue">'+caption+'</span>'

			text = gtk.Label(caption)
			text.set_markup(fancyCaption)
			text.show()
			
			wrapper = gtk.EventBox()
			wrapper.add(text)
			color = gtk.gdk.color_parse('#ffffff')
			wrapper.modify_bg(gtk.STATE_NORMAL, color)
			wrapper.show()
			wrapper.connect('button_press_event', self.buttonPressEvent, pw) 
			
			headSubsizer = gtk.HBox()
			headSubsizer.show()
			headSubsizer.pack_start(wrapper,expand=False,padding=2)
			sizer.pack_start(headSubsizer,expand=False,padding=2)

		#print locks
		for lock in global_pwmanager.getLocksFromArray(rootDir):
			caption = lock["name"]
			fancyCaption = '<span foreground="#0000FF">'+caption+'</span>'

			#highlight the active folder
			if "active" in lock and lock["active"]:
				fancyCaption = '<span foreground="#000000" background="blue">'+caption+'</span>'

			text = gtk.Label(caption)
			text.set_markup(fancyCaption)
			text.show()
			
			wrapper = gtk.EventBox()
			wrapper.add(text)
			color = gtk.gdk.color_parse('#ffffff')
			wrapper.modify_bg(gtk.STATE_NORMAL, color)
			wrapper.show()
			wrapper.connect('button_press_event', self.buttonPressEvent, lock) 
			
			sizer.pack_start(wrapper,expand=False,padding=2)

		for key in global_pwmanager.getKeysFromArray(rootDir):
			caption = key["name"]

                        #set the text colour
                        numShares = len(set(key["share"]+key["inheritedShare"]+key["roShare"]+key["inheritedRoShare"]))
                        if numShares==1:
				fancyCaption = '<span foreground="#1E90FF">'+caption+'</span>'
                        elif numShares<global_pwmanager.glob_config["warnlevel"]:
				fancyCaption = '<span foreground="#B8860B">'+caption+'</span>'
                        else:
				fancyCaption = '<span foreground="#FF1493">'+caption+'</span>'

                        #set nonchangable items to grey
                        if not global_pwmanager.glob_userid in key["share"]+key["inheritedShare"]:
				fancyCaption = '<span foreground="#C8C8C8">'+caption+'</span>'

			#highlight the active folder
			if "active" in key and key["active"]:
				fancyCaption = '<span foreground="#000000" background="blue">'+caption+'</span>'

			text = gtk.Label(caption)
			text.set_markup(fancyCaption)
			text.show()
			
			wrapper = gtk.EventBox()
			wrapper.add(text)
			color = gtk.gdk.color_parse('#ffffff')
			wrapper.modify_bg(gtk.STATE_NORMAL, color)
			wrapper.show()
			wrapper.connect('button_press_event', self.buttonPressEvent, key) 
			
			headSubsizer = gtk.HBox()
			headSubsizer.show()
			headSubsizer.pack_start(wrapper,expand=False,padding=2)
			sizer.pack_start(headSubsizer,expand=False,padding=2)

			caption = key["name"]
			fancyCaption = '<span foreground="#0000FF">'+caption+'</span>'

	def buttonPressEvent(self,widget,event,what):
		if event.button == 1:
			#left click
			self.showDetails(widget,event,what)
			pass
		elif event.button == 3:
			#right click
			self.showContextMenu(widget,event,what)
			pass
		pass

	def showContextMenu(self,widget,event,what):
		menu = gtk.Menu()
		if (what["type"] == "dir"):
			menuItem = gtk.MenuItem("new")
			menuItem.connect("activate",self.new,what)
			menu.append(menuItem)
		menuItem = gtk.MenuItem("delete")
		menuItem.connect("activate",self.delete,what)
		menu.append(menuItem)
		menuItem = gtk.MenuItem("share")
		menuItem.connect("activate",self.share,what)
		menu.append(menuItem)
		menuItem = gtk.MenuItem("copy")
		menuItem.connect("activate",self.copy,what)
		menu.append(menuItem)
		menu.show_all()
		menu.popup(None,None,None,event.button,event.time,None)

	def new(self,widget,what):
		self.removeActive(global_pwmanager.glob_data["pws"])
		what["active"]=1
		global pwDetailPanel
		pwDetailPanel.display(what)
		if (what["type"] == "dir"):
			pwDetailPanel.notebook.set_current_page(1)
		self.draw()

	def delete(self,widget,what):
		self.removeActive(global_pwmanager.glob_data["pws"])
		what["active"]=1
		global pwDetailPanel
		pwDetailPanel.display(what)
		if (what["type"] == "dir"):
			pwDetailPanel.notebook.set_current_page(2)
		if (what["type"] == "pw"):
			pwDetailPanel.notebook.set_current_page(1)
		self.draw()

	def share(self,widget,what):
		self.removeActive(global_pwmanager.glob_data["pws"])
		what["active"]=1
		global pwDetailPanel
		pwDetailPanel.display(what)
		if (what["type"] == "dir"):
			pwDetailPanel.notebook.set_current_page(3)
		if (what["type"] == "pw"):
			pwDetailPanel.notebook.set_current_page(2)
		self.draw()

	def copy(self,widget,what):
		clipboard = gtk.Clipboard()
		self.__clipboardContent = what["password"]
		self.__oldClipboardContent = clipboard.wait_for_text()

		clipboard.set_text(what["password"])

		gobject.timeout_add_seconds(120, self.resetClipboard)

	def resetClipboard(self):
		clipboard = gtk.Clipboard()
		if clipboard.wait_for_text() == self.__clipboardContent:
			clipboard.set_text(self.__oldClipboardContent)
			self.__oldClipboardContent = None

	def __clear(self):
		self.__content.destroy()
		self.__content	= gtk.VBox()
		self.__content.show()
		self.__wrapper.add(self.__content)

        #
        #       the function for (re)drwing the content
        #
        def draw(self):
		global global_pwmanager

		self.__clear()

		box = gtk.VBox()
		box.show()

                text = gtk.Label(str="Ihre Passwoerter:")
		text.show()
		box.pack_start(text,expand=False,padding=4)
                self.printDir(0,box,global_pwmanager.glob_data["pws"])

		self.__content.pack_start(box,expand=False,padding=6)

        #
        #       create the function for showing or hiding the content of a directory
        #
	def showHide(self,widget,additionalArgument):
		if (additionalArgument["open"]):
			additionalArgument["open"]=0
		else:
			additionalArgument["open"]=1
		self.draw()

	def removeActive(self,rootDir):
		global global_pwmanager

                for currentDir in global_pwmanager.getDirsFromArray(rootDir):
			currentDir["active"]=0
			self.removeActive(currentDir["content"])
                for pw in global_pwmanager.getPwsFromArray(rootDir):
			pw["active"]=0
                for lock in global_pwmanager.getLocksFromArray(rootDir):
			lock["active"]=0
                for key in global_pwmanager.getKeysFromArray(rootDir):
			key["active"]=0

        #
        #       create the function for showing the details of a directory
        #
	def showDetails(self,widget,event,additionalArgument):
		#remove previous marks
		self.removeActive(global_pwmanager.glob_data["pws"])

		#mark the folder as active
		additionalArgument["active"]=1

		global pwDetailPanel
		pwDetailPanel.display(additionalArgument)
		self.draw()

#
#       the panel for showing and managing the partners the user coloberates with
#       
class PartnerPanel(gtk.ScrolledWindow):

        #
        #       the constructor
        #
        def __init__(self):
		dialog = gtk.MessageDialog( None,gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT)

		self.__content	= gtk.VBox()

		self.__wrapper = gtk.EventBox()
		self.__wrapper.add(self.__content)
		self.__wrapper.show()

		dialog.vbox.add(self.__wrapper)

		self.draw()

		dialog.show_all()


	def __clear(self):
		self.__content.destroy()
		self.__content	= gtk.VBox()
		self.__content.show()
		self.__wrapper.add(self.__content)

	#
	#	create the function for inviting partners
	#
        def getInvite(self,keyid):
                def invite(event):
			#send invite
                        global_pwmanager.invite(keyid.get_text())

			#redraw panel
			self.draw()
                        pwListPanel.draw()
                return invite

	#
	#	create the function for accepting an invite
	#
        def getAccept(self,keyid):
                def accept(event):
			#accept invite
                        global_pwmanager.acceptPartner(keyid)

			#redraw panel
			self.draw()
                        pwListPanel.draw()
                return accept

	#
	#	create the function for refusing an invite
	#
        def getDeny(self,keyid):
                def deny(event):
			#deny invite
                        global_pwmanager.denyPartner(keyid.get_text())

			#redraw panel
			self.draw()
                        pwListPanel.draw()
                return deny

	#
	#	the function for (re-)drawing the partnerpanel
	#
        def draw(self):
		global global_pwmanager

		#remove old stuff
                self.__clear()

		box = gtk.VBox()
		box.show()

		#information about you
                text = gtk.Label(str="Ihre Userid:")
		text.show()
		box.pack_start(text,expand=False,padding=4)
                text = gtk.Label(str=global_pwmanager.glob_userid+"@"+global_pwmanager.glob_server)
		text.set_selectable(True)
		text.show()
		box.pack_start(text,expand=False,padding=4)

		#information about your partners
                text = gtk.Label(str="Ihre Partner:")
		text.show()
		box.pack_start(text,expand=False,padding=4)
                for user in global_pwmanager.glob_data["users"].keys():
			text = gtk.Label(str=global_pwmanager.glob_data["users"][user]["name"]+"-"+user+"@"+global_pwmanager.glob_data["users"][user]["server"])
			text.show()
			box.pack_start(text,expand=False,padding=4)

		#the button for inviting new partners
		keyid = gtk.Entry()
		keyid.set_size_request(1000,20)

		button = gtk.Button(label='+')
		button.show()
		button.connect("clicked",self.getInvite(keyid))
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)

		box.pack_start(keyid,expand=False,padding=4)

		#the pending invites you send
		if global_pwmanager.glob_data["invited"] != []:
			text = gtk.Label(str="offene Einladungen von Ihnen:")
			text.show()
			box.pack_start(text,expand=False,padding=4)
			for partner in global_pwmanager.glob_data["invited"].keys():
				text = gtk.Label(str=partner+"@"+global_pwmanager.glob_data["invited"][partner]["server"])
				text.show()
				box.pack_start(text,expand=False,padding=4)

		#the pending invites send to you
		if global_pwmanager.glob_data["pending"] != []:
			text = gtk.Label(str="offene Einladungen an Sie:")
			text.show()
			box.pack_start(text,expand=False,padding=4)
			for partner in global_pwmanager.glob_data["pending"].keys():
				text = gtk.Label(str=partner+"@"+global_pwmanager.glob_data["pending"][partner]["server"])
				text.show()
				box.pack_start(text,expand=False,padding=4)

                		button = wx.Button(self, id=-1, label='Accept',pos=(glob_marginLeft,glob_marginTop+count*glob_lineHeight))
                		button.Bind(wx.EVT_BUTTON, self.getAccept(partner))
                		button = wx.Button(self, id=-1, label='Deny',pos=(glob_marginLeft+200,glob_marginTop+count*glob_lineHeight))
                		button.Bind(wx.EVT_BUTTON, self.getDeny(partner))

		self.__content.pack_start(box,expand=False,padding=6)
		self.__content.show_all()

#
#       the panel for global user activities
#
class ActionsPanel():
         
        #
        #       the constructor
        #
        def __init__(self):
		dialog = gtk.MessageDialog( None,gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT)

		self.__content	= gtk.VBox()

		self.__wrapper = gtk.EventBox()
		self.__wrapper.add(self.__content)
		self.__wrapper.show()

		dialog.vbox.add(self.__wrapper)

		self.draw()

		dialog.show_all()

	def __clear(self):
		self.__content.destroy()
		self.__content	= gtk.VBox()
		self.__content.show()
		self.__wrapper.add(self.__content)

	#
	#	create the function for reseting the application to blank state and reload again
	#
	def reset(self,event):
		#delete most stuff
		global_pwmanager.glob_data["pws"] = []
		global_pwmanager.glob_data["pwsnocache"] = []
		global_pwmanager.glob_data["servers"] = {}
		global_pwmanager.glob_data["servers"][global_pwmanager.glob_server] = {}
		global_pwmanager.glob_data["servers"][global_pwmanager.glob_server]["lastid"] = 0
		global_pwmanager.glob_data["servers"][global_pwmanager.glob_server]["cache"] = []
		
		#basic directories
		#TODO: check this. why aren't the users reseted?
		for user in global_pwmanager.glob_data["users"].keys():
			global_pwmanager.glob_data["pwsnocache"].append({"type":"dir","active":0,"name":user,"open":1,"content":[],"realpath":[],"share":[],"inheritedShare":[user],"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":[],"foreignInheritedRoShare":[]})

		#reload
		global_pwmanager.update()

		#redraw panels
		pwListPanel.draw()
		self.draw()

	#
	#	the function for going back to an old state of the application
	#	TODO: Fix or remove
	#
	def revert(self,event):
		#only delete states if necessary
		if global_pwmanager.glob_data["lastid"]>messageNr.get_text():
			#delete most stuff
			global_pwmanager.glob_data["pws"] = []
			global_pwmanager.glob_data["pwsnocache"] = []
			global_pwmanager.glob_data["cache"] = []
			global_pwmanager.glob_data["lastid"] = 0
		
			#basic directories
			for user in global_pwmanager.glob_data["users"].keys():
				global_pwmanager.glob_data["pwsnocache"].append({"type":"dir","active":0,"name":user,"open":1,"content":[],"realpath":[],"share":[],"inheritedShare":[user],"foreignInheritedShare":[],"auth":[],"roShare":[],"inheritedRoShare":[],"foreignInheritedRoShare":[]})

		#stop updates and set the revert Flag
		global_pwmanager.glob_data["revert"] = 1
		global_pwmanager.timer.Stop()

		#reload
		global_pwmanager.update(messageNr.get_text())

		#redraw panels
		pwListPanel.draw()
		self.draw()

	#
	#	create the function for leaving an old state of the application accessed with revert
	#	TODO: Fix or remove
	#
	def removeRevert(self,event):
		global_pwmanager.glob_data["revert"] = 0
		global_pwmanager.timer.Start(glob_pwmanager.glob_config["updateFreq"]*1000)

		#update the aplication
	       	global_pwmanager.update()

		#redraw panels
		pwListPanel.draw()
		self.draw()

	def update(self,event):

		#update the aplication
		global_pwmanager.update()

		#redraw panels
		pwListPanel.draw()
		self.draw()

	#
	#	export the applicationdata and keys to a file
	#
        def exportSetup(self):
		global_pwmanager.exportSetup(wx.FileSelector(flags=wx.FD_SAVE,message="Wohin soll die Datei exportiert werden?",default_path = wx.GetHomeDir(),default_filename = glob_pwmanager.glob_userid+'export.gpg',default_extension = "gpg"))

	#
	#	the function for drawing the actions panel
	#
        def draw(self):
		global global_pwmanager

		box = gtk.VBox()
		box.show()

		#remove old stuff
                self.__clear()

		#the reset button
		button = gtk.Button(label='Reset')
		button.show()
		button.connect("clicked",self.reset)
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)

		#the revert form
		#TODO: Fix, the fact that there are now more than 1 lastid
                #button = wx.Button(self, id=-1, label='Bis Nachricht zurueckgehen:',pos=(glob_marginLeft,glob_marginTop+lineCount*glob_lineHeight))
                #lineCount += 1.7
                #messageNr = wx.TextCtrl(self, -1, str(share.glob_data["lastid"]), (glob_marginLeft,glob_marginTop+lineCount*glob_lineHeight))
                #button.Bind(wx.EVT_BUTTON, self.getRevert(messageNr))

                #disable the revert
                #if global_pwmanager.glob_data["revert"]:
                #        t = wx.StaticText(self, -1, "Updates wurden deaktiviert, um zu einer alten Nachricht zurueckzugehen", (glob_marginLeft,glob_marginTop+lineCount*glob_lineHeight))
                #        lineCount += 1
                #        t.SetForegroundColour((238,0,0))
                #        button = wx.Button(self, id=-1, label='Updates wieder aktivieren',pos=(glob_marginLeft,glob_marginTop+lineCount*glob_lineHeight))
                #        button.Bind(wx.EVT_BUTTON, self.getRemoveRevert())
                #        lineCount += 1.7

		#the update button
		button = gtk.Button(label='Update')
		button.show()
		button.connect("clicked",self.update)
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)

		#the export button
		button = gtk.Button(label='Export')
		button.show()
		button.connect("clicked",self.exportSetup)
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)

		self.__content.pack_start(box,expand=False,padding=6)
		self.__content.show_all()

#
#	the panel for manipulating the global settings
#
class SettingsPanel():
         
        #
        #       the constructor
        #
        def __init__(self):
		dialog = gtk.MessageDialog( None,gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT)

		self.__content	= gtk.VBox()

		self.__wrapper = gtk.EventBox()
		self.__wrapper.add(self.__content)
		self.__wrapper.show()

		dialog.vbox.add(self.__wrapper)

		self.draw()

		dialog.show_all()

	def __clear(self):
		self.__content.destroy()
		self.__content	= gtk.VBox()
		self.__content.show()
		self.__wrapper.add(self.__content)

	#
	#	create the function for setting the warnLevel
	#
        def setLevel(self,event,warnLevel):
		global_pwmanager.glob_config["warnlevel"] = int(warnLevel.get_text())

		#redraw panels
		pwListPanel.draw()
		self.draw()

	#
	#	create the function for setting the update frequency
        def setFreq(self,event,updateFreq):
		global_pwmanager.glob_config["updateFreq"] = int(updateFreq.get_text())
		
		#TODO: port to gtk
		#global_pwmanager.timer.Stop()
		#global_pwmanager.timer.Start(global_pwmanager.glob_config["updateFreq"]*1000)
		
		#redraw panels
		self.draw()

	#
	#	the function for drawing the formulas for changing the settings
	#
        def draw(self):
		#remove old stuff
                self.__clear()

		global global_pwmanager

		box = gtk.VBox()
		box.show()


		#the form for the warnlevel
		warnLevel = gtk.Entry()
		warnLevel.set_size_request(400,20)
		warnLevel.set_text(str(global_pwmanager.glob_config["warnlevel"]))
		button = gtk.Button(label='Warnlevel fuer Shares:')
		button.show()
		button.connect("clicked",self.setLevel,warnLevel)
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)
		box.pack_start(warnLevel,expand=False,padding=4)

		#the form for the update frequenzy
		updateFreq = gtk.Entry()
		updateFreq .set_size_request(400,20)
		updateFreq .set_text(str(global_pwmanager.glob_config["updateFreq"]))
		button = gtk.Button(label='Updatefrequenz:')
		button.show()
		button.connect("clicked",self.setFreq,updateFreq)
		button.set_size_request(200,20)
		box.pack_start(button,expand=False,padding=4)
		box.pack_start(updateFreq,expand=False,padding=4)

		self.__content.pack_start(box,expand=False,padding=6)
		self.__content.show_all()
