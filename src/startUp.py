#! /usr/bin/python

#
#	The graphical Userinterface for sharingsecrets 
#

import pygtk
pygtk.require('2.0')
import gtk

import comm
import sharingsecretsFrontend as sharingsecretsFrontend
import getpass
import users
import json
import traceback

class MainFrame(gtk.HPaned):
	def __init__(self,user):
		gtk.HPaned.__init__(self)
		self.show()
		self.user=user

def startMainApplication(user):
        if user.authentificated:
		window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window.set_title("sharingsecrets")
		window.maximize()
		window.show_tabs = True
		window.show_border = True

		menuBar = gtk.MenuBar()
		menu = gtk.Menu()

		partners = gtk.MenuItem("partners")
		settings = gtk.MenuItem("settings")
		actions = gtk.MenuItem("actions")

		pwmanager = comm.sharingsecretsBackend(user)
                mainFrame = sharingsecretsFrontend.PasswordPanel(pwmanager)
		
		menuBar.append(partners)
		menuBar.append(settings)
		menuBar.append(actions)

		vBox = gtk.VBox(False,0)
		window.add(vBox)

		vBox.pack_start(menuBar,False,False,2)
		vBox.pack_start(mainFrame)

		settings.connect("activate", showSettings)
		#settings.connect("clicked", showSettings)
		partners.connect("activate", showPartners)
		#partners.connect("clicked", showPartners)
		actions.connect("activate", showActions)
		#actions.connect("clicked", showActions)

		window.show_all()
		window.connect("destroy", gtk.main_quit)
		gtk.main()

def showSettings(event):
	 sharingsecretsFrontend.SettingsPanel()

def showPartners(event):
	 sharingsecretsFrontend.PartnerPanel()

def showActions(event):
	 sharingsecretsFrontend.ActionsPanel()

#
#	start by forcing the user to login
#
def startProgram():
	#get the user
	user = users.displayLogin()

	#build neccessary folders
	import os
	if not os.path.exists("./data/"):
		os.makedirs("./data")
	if not os.path.exists("./data/"+user.userid):
		os.makedirs("./data/"+user.userid)
	if not os.path.exists("./data/"+user.userid+"/sharingsecrets/"):
		os.makedirs("./data/"+user.userid+"/sharingsecrets/")
	if not os.path.exists("./config/"):
		os.makedirs("./config/")
	if not os.path.exists("./config/"+user.userid):
		os.makedirs("./config/"+user.userid)
	
	#start the application
	startMainApplication(user)
