from gnupg import GPG

import pygtk
pygtk.require('2.0')
import gtk

import json

def storeLocalUsers(localUsers):
	f = open("data/localUsers.json",'w')
	f.write( json.dumps(localUsers) )
	f.close()

#       parameter:
#       return:
#
def loadLocalUsers():
        #load the users
	f = open("data/localUsers.json",'r')
	localUsers = f.read()
	localUsers = json.loads(localUsers)
	f.close()
	return localUsers

def displayLogin():
	#the users available
	localUsers = loadLocalUsers()
	user = User() #HACK: empty user

	#display login window
	window = gtk.Window(gtk.WINDOW_TOPLEVEL)
	window.set_size_request(262,170)
	window.set_position(gtk.WIN_POS_CENTER)
	window.set_title("login")
	loginFrame = LoginFrame( localUsers, user ) #HACK: fills the user by reference

	window.add(loginFrame)
	window.show()

	gtk.main()
	window.destroy()

	if user.username:
		localUsers = loadLocalUsers()
		localUsers["lastActive"] = user.username
		storeLocalUsers(localUsers)

	return user


class User():
        def __init__(self):
		self.userid = None
		self.passphrase = None
		self.username = None
		self.authentificated = False

	def testPassword(self):
	        gpg = GPG()

		#the text to test on
		plain = "test"

		#encrypt the testtext
		crypted = gpg.encrypt(plain,self.userid,always_trust=True)

		#decrypt the testext
		uncrypted = str(gpg.decrypt(str(crypted),passphrase=self.passphrase))

		#check if text was succsessfully encrypted and decrypted
		if (plain == uncrypted):
			return True
		return False

class LoginFrame(gtk.Layout):
        def __init__(self,localUsers,user):
		gtk.Layout.__init__(self)
		self.localUsers = localUsers["users"]
		self.lastActive = localUsers["lastActive"]
		self.user = user

		self.draw()

	def draw(self):
		self.show()

		#the user selection
		self.keyid = gtk.combo_box_new_text()
		self.keyid.show()
		counter = 0
		for item in sorted(self.localUsers.keys()):
		        self.keyid.append_text(item)
			if item == self.lastActive:
				self.keyid.set_active(counter)
			counter = counter + 1
		self.keyid.set_size_request(200,24)
		self.put(self.keyid, 30, 30)
        
		#the pasword field
		self.pw = gtk.Entry()
		self.pw.show()
		self.pw.set_size_request(200,24)
		self.pw.connect("activate", self.login)
		self.pw.set_visibility(gtk.FALSE)
		self.put(self.pw, 30, 60)
		self.pw.grab_focus()
        	
		#the login button
		button = gtk.Button(label="login")
		button.show()
		button.set_size_request(70,24)
		button.connect("clicked",self.login)
		self.put(button, 30,90)

		#the import button
		button = gtk.Button(label="neu")
		button.show()
		button.set_size_request(70,24)
		button.connect("clicked",self.new)
		self.put(button, 160,130)

		#the new button
		button = gtk.Button(label="import")
		button.show()
		button.set_size_request(70,24)
		button.connect("clicked",self.importUser)
		self.put(button, 30,130)

		self.show_all()
	
	def importUser(self,widget):
		pass

	def new(self,widget):

		dialog = gtk.MessageDialog( None,gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,gtk.MESSAGE_QUESTION)

		dialog.set_markup('Das Generieren ihres Keys wird einige Zeit in Anspruch nehmen, trinken Sie einen Kaffe oder verhalten Sie sich zufaellig.')

		hbox = gtk.VBox()

		hbox.pack_start(gtk.Label("Name:"),False, 5, 5)
		text = gtk.Label()
		text.set_markup("<small>unter diesem Namen werden Sie bei anderen angezeigt</small>")
		hbox.pack_start(text,False, 5, 5)
		name = gtk.Entry()
		name.show()
		hbox.pack_start(name, False, 5, 5)
		
		hbox.pack_start(gtk.Label("Passwort:"), False, 5, 5)
		text = gtk.Label()
		text.set_markup("<small>Mit diesem Passwort wird der GPG Key von organize! vor lokalen Zugriff geschuetzt. Dieser Key ist Ihr eigentliches Mittel zu Authentifizierung, ohne diesen Key reicht ihr username/Passwort nicht zum Login!</small>")
		hbox.pack_start(text,False, 5, 5)
		password = gtk.Entry()
		password.show()
		hbox.pack_start(password)

		hbox.pack_start(gtk.Label("Server:"), False, 5, 5)
		text = gtk.Label()
		text.set_markup("<small>dieser Server wird genutzt um ihre Passwoerter zu speichern</small>")
		hbox.pack_start(text,False, 5, 5)
		server = gtk.Entry()
		server.show()
		hbox.pack_start(server)

		dialog.vbox.pack_end(hbox, True, True, 0)
		dialog.show_all()

		name.connect("activate", self.createUser, dialog, name, password, server)
		password.connect("activate",self.createUser, dialog, name, password, server)
		server.connect("activate", self.createUser, dialog, name, password, server)

	def createUser(self, event, dialog, username, password, server):
		import comm
		fingerprint = comm.createUser(username.get_text(),password.get_text(),server.get_text())

		#add key to thelist of users working with this installation
		users = loadLocalUsers()
		users["users"][username.get_text()] = {}
		users["users"][username.get_text()]["fingerprint"] = fingerprint
		users["users"][username.get_text()]["server"] = server.get_text()
		storeLocalUsers(users)

		self.user.username   = username.get_text()
		self.user.userid     = fingerprint
		self.user.passphrase = password.get_text()
		self.user.server     = server.get_text()

		dialog.destroy()

		self.user.authentificated = True

		gtk.mainquit()

	def login(self, widget):
		#get basic data for opening the cryptostuff
		if not self.localUsers.has_key(self.keyid.get_active_text()):
			dialog = gtk.MessageDialog(message_format='der angegebene User ist nicht bekannt', 
				buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_ERROR)
			dialog.run()
			dialog.destroy()
			self.keyid.grab_focus()
			return

		self.user.username   = self.keyid.get_active_text()
		self.user.userid     = self.localUsers[self.user.username]["fingerprint"]
		self.user.passphrase = self.pw.get_text()
		self.user.server     = self.localUsers[self.user.username]["server"]

		#check password
		if not self.user.testPassword():
			dialog = gtk.MessageDialog(message_format='die angegebene Passphrase konnte nicht verifiziert werden', 
				buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_ERROR)
			dialog.run()
			dialog.destroy()
			self.pw.grab_focus()
			return

		self.user.authentificated = True

		gtk.mainquit()
